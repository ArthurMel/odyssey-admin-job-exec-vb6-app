Attribute VB_Name = "modCrystalCommon"
Option Explicit

Public crxiApplication               As CRAXDRT.Application
Public crxiReport                    As CRAXDRT.Report
Public crxiParameterField            As CRAXDRT.ParameterFieldDefinition
Public crxiTables                    As CRAXDRT.DatabaseTables
Public crxiTable                     As CRAXDRT.DatabaseTable
Public crxiDatabaseFieldDefinitions  As CRAXDRT.DatabaseFieldDefinitions
Public crxiDatabaseFieldDefinition   As CRAXDRT.DatabaseFieldDefinition
Public crxiSecs                      As CRAXDRT.Sections
Public crxiSec                       As CRAXDRT.Section
Public crxiRepObjs                   As CRAXDRT.ReportObjects
Public crxiSubRepObj                 As CRAXDRT.SubreportObject
Public crxiSubReport                 As CRAXDRT.Report
'Output formats
Global Const cri_maxFmts                    As Long = 32
Global cri_numFmts                          As Long
Global cri_defFmtIdx                        As Long
Global cri_ofVal(0 To cri_maxFmts - 1)      As Long
Global cri_ofDesc(0 To cri_maxFmts - 1)     As String
Global cri_ofExt(0 To cri_maxFmts - 1)      As String
Global cri_ofConst(0 To cri_maxFmts - 1)    As String
Global cri_ofSupported(0 To cri_maxFmts - 1) As Boolean

Enum reportRunMode
    runOnLine = 9
    runToPrinter = 0
    runToFax = 1
    runToEmail = 2
    runToDisk = 3
    runToDiskMultiple = 4
    runNowhere = -1
End Enum
Dim reportRunMode                   As Long

Private Declare Function ShellExecute _
                Lib "shell32.dll" _
                Alias "ShellExecuteA" _
        (ByVal hwnd As Long, _
            ByVal lpOperation As String, _
            ByVal lpFile As String, _
            ByVal lpParameters As String, _
            ByVal lpDirectory As String, _
            ByVal nShowCmd As Long) As Long

Private Declare Function GetDesktopWindow Lib "user32" () As Integer

Public Sub RunCrystalReport( _
                        jID As Long, _
                        jobSeqNum As Long, _
                        eTxt As String, _
                        logHistMessage As Integer, _
                        nRecs As Long, _
                        Optional optOnLine As Boolean)

'ADO Command variables
    Dim rs                      As ADODB.Recordset
    Dim rs2                     As ADODB.Recordset
    Dim rs3                     As ADODB.Recordset
    Dim rsOpen                  As Boolean
    Dim rs2Open                 As Boolean
    Dim rs3Open                 As Boolean
    Dim rsSQL                   As String
    Dim f                       As ADODB.Field
'Scheduler job format details
    Dim tDeliveryID             As Long
    Dim tJobDesc                As String
    Dim tJobFormula             As String
    Dim tJobFileName            As String
    Dim tJobDestPrinter         As String
    Dim tCopies                 As Long
    Dim tJobDestEmail           As String
    Dim tJobDestFax             As String
    Dim tJobDestFile            As String
    Dim tJobDestType            As Long
    Dim tJobDestFormat          As Long
    Dim tPort                   As String
    Dim tDriver                 As String
    Dim tPaperOrientation       As String
    Dim tPaperSource            As String
    Dim tPaperSize              As String
    Dim tDatabase               As String
    Dim tDestSubject            As String
    
'Scheduler error messaging details
    Dim tem(0 To 1)             As Long
    Dim tema(0 To 1)            As String
    Dim emsg                    As String
    
    Dim tFldr                   As String
    Dim tFnme                   As String
    Dim tSffx                   As String
    
    Dim rMsg                    As String
    
    Dim runMode                 As reportRunMode
    Dim onlineMode              As Boolean
    Dim sts                     As Long
    
    Dim x                       As String
    Dim runOpts                 As String
    Dim t                       As String
    Dim Pos                     As Long
    Dim i                       As Long
    Dim d                       As Double
    Dim b                       As Boolean
    Dim eNm                     As Long
    
    Dim rw                      As Long
    Dim cl                      As Long
    
    Dim hID                     As Long
    Dim rTim                    As Long
    Dim sTim                    As Date
    Dim eTim                    As Date
    Dim pEnum                   As Long
    Dim pEtxt                   As String
    Dim rStat                   As Long
    Dim dStat                   As Long
    Dim osp                     As Integer
    Dim tmpFileName             As String
    Dim WshNetwork              As Variant
    Dim hwnd                    As Long
    Dim lngShellReturn          As Long
    Dim lngCloseWindowReturn    As Long
    Dim Sender                  As String
    Dim emailAddr               As String
    Dim messageText             As String
    Dim arrAttach(0)            As String
    Dim fso                     As FileSystemObject

'Crystal report parameter variables
    Const maxPars               As Long = 512
    Dim parArry(0 To maxPars)   As String
    Dim pIdx                    As Long
    Dim srtArry(0 To maxPars)   As String
    Dim sIdx                    As Long
    Dim subArry(0 To maxPars)   As String
    Dim subIdx                  As Long
    Dim grpArry1(0 To maxPars)  As String
    Dim grpArry2(0 To maxPars)  As String
    Dim gIdx                    As Long
    
    On Error GoTo RCR_ERR
    rsOpen = False
    eTxt = ""
    onlineMode = False
    If Not IsMissing(optOnLine) Then
        onlineMode = CBool(optOnLine)
    End If
    
    rsSQL = "select * from vw_AR_JobsExplode where arjob_id=" & jID
    
    Set rs = New ADODB.Recordset
    rs.Open rsSQL, gADOconn, adOpenDynamic, adLockReadOnly
    rsOpen = True
    With rs
        
        If .EOF Then
            App.LogEvent "JobExec - Error locating job# " & jID, vbLogEventTypeInformation
            GoTo RCR_END
        End If
        
        If ![arjt_id] = 1 Or ![arjt_id] = 4 Then
            x = Trim$(NullToS(![arpt_folder]))
            If Len(x) > 0 Then
                If Right$(x, 1) <> "\" Then x = x & "\"
            End If
            tJobFileName = x & ![arpt_filename]
            tJobDesc = ![arjob_desc]
            tJobDestType = ![arjob_dest_type]
            If IsNull(![arjob_dest_format]) Then
                tJobDestFormat = cri_defFmtIdx
            Else
                tJobDestFormat = ![arjob_dest_format]
            End If
            
            tJobDestFile = "c:\data\odyssey\temp"
            
            'Only retrieve the first 70 chars from the Job Name
            'There is a limit (218 chars) to the number of characters that can be contained in the path and file name.
            'See http://support.microsoft.com/kb/213983
            tmpFileName = Left(NullToS(![arjob_desc]), 70) & "Job" & jID & "_" & ShortDateTime(Now) & NullToS(![dest_format_FileType])
            rsSQL = "select * from auto_report_defaults where ardef_id=1"
            Set rs2 = New ADODB.Recordset
            rs2.Open rsSQL, gADOconn, adOpenForwardOnly, adLockReadOnly
            If Not rs2.EOF Then
                tJobDestFile = NullToS(rs2![ardef_report_path])
            End If
            rs2.Close
            Set rs2 = Nothing
            tJobDestFile = tJobDestFile & "\" & tmpFileName
            
            tDestSubject = NullToS(![arjob_dest_subject])
            PassArgumentFormula NullToS(![arjob_formula]), tJobFormula, 0, sts
            tPaperSource = NullToZero(![arpt_papersource])
            tPaperOrientation = NullToZero(![arpt_paperorientation])
            tPaperSize = NullToZero(![arpt_papersize])
            tem(0) = NullToZero(![arjob_emailtype_1])
            tem(1) = NullToZero(![arjob_emailtype_2])
            tema(0) = NullToS(![arjob_emailto_1])
            tema(1) = NullToS(![arjob_emailto_2])
            tDatabase = ![arjob_database]
            If tDatabase = "{default}" Then tDatabase = gCrystalDB
            If onlineMode Then runMode = runOnLine
            
            If ![arjt_id] = 1 Then  'Crystal Report
            
                pIdx = 0
                sIdx = 0
                gIdx = 0
                subIdx = 0
                Do Until .EOF
                    If Not IsNull(![arjobp_par]) Then
                        pIdx = pIdx + 1
                        parArry(pIdx) = ![arjobp_par]
                        pIdx = pIdx + 1
                        parArry(pIdx) = DecodeArgumentFunction(dStat, NullToS(![arjobp_value]))
                        If dStat > 1 Then
                            eTxt = ARGStatMsg(dStat)
                            GoTo RCR_END
                        End If
                    End If
                    .MoveNext
                Loop
            
                rsSQL = "select * from auto_report_job_subrpt where arjob_id=" & jID
                Set rs2 = New ADODB.Recordset
                rs2.Open rsSQL, gADOconn, adOpenForwardOnly, adLockReadOnly
                rs2Open = True
                If Not rs2.EOF Then
                    Do Until rs2.EOF
                        gIdx = gIdx + 1
                        grpArry1(gIdx) = NullToS(rs2![arjob_sub_report])
                        grpArry2(gIdx) = NullToS(rs2![arjob_sel_formula])
                        rs2.MoveNext
                    Loop
                End If
                rs2.Close
                Set rs2 = Nothing
                
                If Not cri_RptOpen(tJobFileName, UCase(tDatabase)) Then GoTo RCR_END
                If Not cri_SetCriteria(grpArry1, grpArry2, gIdx, _
                                tJobFormula, "", _
                                parArry, pIdx, srtArry, sIdx) Then GoTo RCR_END
    
                sts = 0
                sts = cri_SaveReport(tJobDesc, sTim, eTim, pEnum, pEtxt, _
                    tJobDestFormat, tJobDestEmail, tDestSubject, _
                    tJobDestFile)
                If sts = 42 Then sts = 0 'a status of 42 is a valid response
                
                rTim = DateDiff("s", sTim, eTim)
                
                If sts <> 1 Then 'Error occured
                    rMsg = "Job failed. Error: " & _
                            pEnum & " " & Trim$(Left$(pEtxt, 220))
                    rStat = 130
                    nRecs = 0
                    GoTo RCR_LOGMSG
                End If
                
                On Error Resume Next
                
                With crxiReport.PrintingStatus
                    rMsg = "Recs read=" & .NumberOfRecordRead & _
                    ", Recs printed=" & .NumberOfRecordPrinted & _
                    ", Recs selected=" & .NumberOfRecordSelected & _
                    ", Pages=" & .NumberOfPages
                    
                    nRecs = .NumberOfRecordPrinted
                    If Err <> 0 Then
                        rMsg = "Job failed. Error: " & _
                            Err.Number & " " & Trim$(Left$(Err.Description, 220))
                        rStat = 130
                        nRecs = 0
                        GoTo RCR_LOGMSG
                    End If
                    
                    On Error GoTo RCR_ERR
                    
                    If .NumberOfRecordRead = 0 Then
                        rStat = 140
                    Else
                        rStat = 100
                    End If
                End With
                
                rsSQL = "select * from dbo.auto_report_job_dest where arjob_id=" & jID
                Set rs2 = New ADODB.Recordset
                rs2.Open rsSQL, gADOconn, adOpenForwardOnly, adLockReadOnly
                If Not rs2.EOF Then
                    Do Until rs2.EOF
                        If rs2![arjob_dest_type] = 0 Then
                            'Printer
                            Set WshNetwork = CreateObject("WScript.Network")
                            WshNetwork.SetDefaultPrinter rs2![arjob_dest_printer_name]
                            For i = 1 To CInt(rs2![arjob_dest_printer_copies])
                                lngShellReturn = ShellExecute(hwnd, "Print", tJobDestFile, "", App.Path, 0)
                                If lngShellReturn > 32 Then
                                    'OK
                                Else
                                    rMsg = "Job failed to print to " & tJobDestPrinter
                                    rStat = 130
                                    nRecs = 0
                                    GoTo RCR_LOGMSG
                                End If
                            Next i
                            Set WshNetwork = Nothing
                        ElseIf rs2![arjob_dest_type] = 1 Then
                            'Fax
                            Sender = "job.scheduler@aptouring.com.au"
                            emailAddr = "fax.maker@aptouring.com.au"
                            messageText = "::p=low" & vbCrLf & "::,Please see attachment for results...," & Trim(FixPhone(rs2![arjob_dest_fax]))
                            arrAttach(0) = tJobDestFile
                            Call EmailFile(25, "smtp_cheltenham", Sender, emailAddr, "", tDestSubject, messageText, arrAttach(), eNm, eTxt)
                        ElseIf rs2![arjob_dest_type] = 2 Then
                            'Email
                            Sender = "job.scheduler@aptouring.com.au"
                            emailAddr = rs2![arjob_dest_email]
                            messageText = "Please see attached." & vbCrLf & vbCrLf & "This is an automated email and we are unable to respond to replies.  If you require further assistance please contact us by emailing help.reports@aptouring.com.au and quote the Job number."
                            arrAttach(0) = tJobDestFile
                            Call EmailFile(25, "smtp_cheltenham", Sender, emailAddr, "", tDestSubject, messageText, arrAttach(), eNm, eTxt)
                        ElseIf rs2![arjob_dest_type] = 3 Then
                            'Disk
                            Set fso = New FileSystemObject
                            fso.CopyFile tJobDestFile, rs2![arjob_dest_disk_path] & "\" & tmpFileName, True
                            Set fso = Nothing
                        End If
                        
                        rs2.MoveNext
                    Loop
                End If
                rs2.Close
                Set rs2 = Nothing
                
            Else    'SQL Query
            
                sts = 0
                sTim = Now
                
                SetADOConn gDBconn, tDatabase, gCrystalUser, gCrystalPwd, gDBconnOpen
                If Not gDBconnOpen Then
                    EHandlerADO Err.Number, Err.Description, "Cannot open datasource"
                    GoTo RCR_ERR
                End If
                
                rsSQL = ![arjob_formula]
                Set rs3 = New ADODB.Recordset
                gDBconn.CommandTimeout = 1200   '20 Minutes
                rs3.Open rsSQL, gDBconn, adOpenForwardOnly, adLockReadOnly
                rs3Open = True
                If Not rs3.EOF Then
    
                    If Len(Dir(tJobDestFile)) > 0 Then Kill tJobDestFile
                    
                    Set gExcelApp = Nothing
                    
                    Set gExcelApp = CreateObject("Excel.Application")
                    gExcelApp.Visible = False
                    gExcelApp.Workbooks.Add
                    gExcelApp.ActiveSheet.PageSetup.Orientation = xlLandscape
                
                    rw = 1
                    With rs3
                        cl = 1
                        If Not .EOF Then
                            For Each f In .Fields
                                gExcelApp.Cells(rw, cl).Value = f.Name
                                cl = cl + 1
                            Next
                            rw = rw + 1
                        End If
                        Do Until .EOF
                            cl = 1
                            For Each f In .Fields
                                gExcelApp.Cells(rw, cl).Value = CStr(NullToS(f.Value))
                                cl = cl + 1
                            Next
                            nRecs = nRecs + 1
                            .MoveNext
                            rw = rw + 1
                        Loop
                    End With
                    
                    rMsg = tJobDestFile & " created. Records=" & nRecs
                    
                    If nRecs = 0 Then
                        rStat = 140
                    Else
                        rStat = 100
                    End If
                    
                    With gExcelApp
                        .ActiveWorkbook.SaveAs FileName:=tJobDestFile _
                        , FileFormat:=xlNormal, Password:="", WriteResPassword:="", _
                        ReadOnlyRecommended:=False, CreateBackup:=False
                        .Quit
                    End With
                    
                    Set gExcelApp = Nothing
                                
                    rsSQL = "select * from dbo.auto_report_job_dest where arjob_id=" & jID
                    Set rs2 = New ADODB.Recordset
                    rs2.Open rsSQL, gADOconn, adOpenForwardOnly, adLockReadOnly
                    If Not rs2.EOF Then
                        Do Until rs2.EOF
                            If rs2![arjob_dest_type] = 0 Then
                                'Printer
                                Set WshNetwork = CreateObject("WScript.Network")
                                WshNetwork.SetDefaultPrinter rs2![arjob_dest_printer_name]
                                For i = 1 To CInt(rs2![arjob_dest_printer_copies])
                                    lngShellReturn = ShellExecute(hwnd, "Print", tJobDestFile, "", App.Path, 0)
                                    If lngShellReturn > 32 Then
                                        'OK
                                    Else
                                        rMsg = "Job failed to print to " & tJobDestPrinter
                                        rStat = 130
                                        nRecs = 0
                                        GoTo RCR_LOGMSG
                                    End If
                                Next i
                                Set WshNetwork = Nothing
                            ElseIf rs2![arjob_dest_type] = 1 Then
                                'Fax
                                Sender = "job.scheduler@aptouring.com.au"
                                emailAddr = "fax.maker@aptouring.com.au"
                                messageText = "::p=low" & vbCrLf & "::,Please see attachment for results...," & Trim(FixPhone(rs2![arjob_dest_fax]))
                                arrAttach(0) = tJobDestFile
                                Call EmailFile(25, "smtp_cheltenham", Sender, emailAddr, "", tDestSubject, messageText, arrAttach(), eNm, eTxt)
                            ElseIf rs2![arjob_dest_type] = 2 Then
                                'Email
                                Sender = "job.scheduler@aptouring.com.au"
                                emailAddr = rs2![arjob_dest_email]
                                messageText = "Please see attached." & vbCrLf & vbCrLf & "This is an automated email and we are unable to respond to replies.  If you require further assistance please contact us by emailing help.reports@aptouring.com.au and quote the Job number."
                                arrAttach(0) = tJobDestFile
                                Call EmailFile(25, "smtp_cheltenham", Sender, emailAddr, "", tDestSubject, messageText, arrAttach(), eNm, eTxt)
                            ElseIf rs2![arjob_dest_type] = 3 Then
                                'Disk
                                Set fso = New FileSystemObject
                                fso.CopyFile tJobDestFile, rs2![arjob_dest_disk_path] & "\" & tmpFileName, True
                                Set fso = Nothing
                            End If
                            
                            rs2.MoveNext
                        Loop
                    End If
                    rs2.Close
                    Set rs2 = Nothing
                                
                End If
                
                If nRecs = 0 Then
                    rStat = 140
                Else
                    rStat = 100
                End If
                
                eTim = Now
                rTim = DateDiff("s", sTim, eTim)
                
            End If
        Else
            'Run application
            sTim = Now
            runOpts = NullToS(![ara_run_options])
            PassArgumentFormula NullToS(![arjob_formula]), tJobFormula, 1, sts
            If CalcLen(tJobFormula) > 0 Then
                runOpts = tJobFormula
            End If
            i = InStr(1, UCase(runOpts), "ARG_FILE(")
            If i > 0 Then
                t = Left$(runOpts, i - 1)
                x = Right$(runOpts, Len(runOpts) - i + 1)
                Pos = i
                i = InStr(1, x, " ")
                Pos = Pos + i
                x = Left$(x, i - 1)
                i = InStr(1, UCase(x), "JOBSEQ()")
                If i > 0 Then
                    Mid$(x, i, 8) = "JOBSEQ()"
                    x = Replace(x, "JOBSEQ()", jobSeqNum)
                End If
                i = InStr(1, UCase(x), "JOBID()")
                If i > 0 Then
                    Mid$(x, i, 7) = "JOBID"
                    x = Replace(x, "JOBID()", jID)
                End If
                tJobDestFile = DecodeArgumentFunction(i, x)
                If i > 1 Then
                    eTxt = ARGStatMsg(i)
                    GoTo RCR_END
                End If
                t = t & tJobDestFile & " " & Right$(runOpts, Len(runOpts) - Pos + 1)
                runOpts = t
            End If
            runOpts = runOpts & " -APPID " & ![arpt_id]
            x = ![ara_folder]
            If Right$(x, 1) <> "\" Then x = x & "\"
            x = x & ![ara_filename]
                    
            d = Shell(x & " " & runOpts, vbNormalFocus)
            
            rStat = 100
            rMsg = "See application log for details."
            eTim = Now
            rTim = DateDiff("s", sTim, eTim)
        End If
    End With
    
RCR_LOGMSG:
    If logHistMessage = vbYes Then
        LogRunHistory jID, Now, rStat, rMsg, rTim, hID
    End If
    
'   Send confirmation messages
    emsg = ""
    If rStat = 140 Then
        emsg = "Job completed, but no data found."
    Else
        If rStat = 100 Then
            emsg = "Job completed without errors."
        Else
            If rStat = 130 Then
                emsg = "Job failed. See message for error desciption."
            End If
        End If
    End If
    
    If Len(emsg) > 0 Then
        For i = 0 To 1
            If tem(i) > 0 Then
                b = False
                Select Case tem(i)
                    Case 1  'Success
                        If rStat = 140 Or rStat = 100 Then b = True
                    Case 2  'Failure
                        If rStat = 130 Then b = True
                    Case 3  'Both
                        b = True
                End Select
                If b Then
                    EmailFile gSMTPport, gSMTPserver, gSMTPfrom, _
                        tema(i), "", _
                        tJobDesc & " - " & DecodeRunStatus(rStat), _
                        rMsg, arrAttach(), eNm, eTxt
                End If
            End If
        Next i
    End If
    
RCR_END:
    On Error Resume Next
    If rsOpen Then
        rsOpen = False
        rs.Close
    End If
    If rs2Open Then
        rs2Open = False
        rs2.Close
    End If
    If rs3Open Then
        rs3Open = False
        rs3.Close
    End If
    Set rs = Nothing
    Set rs2 = Nothing
    Set rs3 = Nothing
    Set crxiReport = Nothing
    Set crxiApplication = Nothing
    Exit Sub
RCR_ERR:
    eTxt = Err.Description
    EHandlerADO Err.Number, Err.Description
    GoTo RCR_END
End Sub

Public Function FixPhone(ByVal inTxt$) As String
    'Def: Eliminates '-' and ' ' from a phone #
    'Input
    'inTxtx: Phone number
    'Return Value - phone # as string

Dim done As Integer
Dim Pos%
Dim startat As Integer
Dim tmpstr$

    done = False
    startat = 1
    tmpstr$ = inTxt$
    Do Until done
        Pos% = InStr(startat, tmpstr$, "-")
        If Pos% > 0 Then
            tmpstr$ = Left$(tmpstr$, Pos% - 1) & Mid(tmpstr$, Pos% + 1)
            startat = Pos% + 1
        Else
            Pos% = InStr(startat, tmpstr$, " ")
            If Pos% > 0 Then
                tmpstr$ = Left$(tmpstr$, Pos% - 1) & Mid(tmpstr$, Pos% + 1)
                startat = Pos% + 1
            Else
                done = True
            End If
        End If
    Loop
    
    FixPhone = tmpstr$

End Function

Function ShortDateTime(dtmTime)

Dim strYear As String
Dim strMonth As String
Dim strDay As String
Dim strHour As String
Dim strMinute As String
Dim strSecond As String

    strYear = Year(dtmTime)
    strMonth = Right("0" & Month(dtmTime), 2)
    strDay = Right("0" & Day(dtmTime), 2)
    strHour = Right("0" & Hour(dtmTime), 2)
    strMinute = Right("0" & Minute(dtmTime), 2)
    strSecond = Right("0" & Second(dtmTime), 2)
     
    ShortDateTime = strYear & strMonth & strDay & strHour & strMinute & strSecond
End Function

Private Function MatchPrinterName(strPtr As String) As Long

    Dim b       As Boolean
    Dim p       As Printer
    Dim i       As Integer
    Dim x       As String
    
    i = 0
    b = False
    x = UCase$(strPtr)
    MatchPrinterName = -1
    Do Until b Or i > Printers.Count - 1
        If UCase$(Printers(i).DeviceName) = x Then
            b = True
            MatchPrinterName = i
        Else
            i = i + 1
        End If
    Loop
    If Not b Then i = -1
End Function
Public Sub LogRunHistory( _
                            jID As Long, _
                            rDate As Date, _
                            rStat As Long, _
                            rMsg As String, _
                            sDiff As Long, _
                            hID As Long)

    Dim rs              As ADODB.Recordset
    Dim rsSQL           As String
    Dim rsOpen          As Boolean
    Dim osp             As Integer
    
    On Error GoTo LRH_ERR
    
    rsOpen = False
    hID = 0
    rsSQL = "select * from auto_report_run_hist where arhst_id=-1"
    Set rs = New ADODB.Recordset
    rs.Open rsSQL, gADOconn, adOpenDynamic, adLockOptimistic
    rsOpen = True
    
    With rs
        .AddNew
        ![arjob_id] = jID
        ![arhst_run_date] = rDate
        ![arhst_run_msg] = rMsg
        ![arhst_run_time_sec] = sDiff
        ![arrunstat_id] = rStat
        .Update
        hID = ![arhst_id]
    End With
    
LRH_END:
    On Error Resume Next
    If rsOpen Then
        rsOpen = False
        rs.Close
    End If
    Set rs = Nothing
    Exit Sub
LRH_ERR:
    EHandlerADO Err.Number, Err.Description
    GoTo LRH_END
End Sub
Public Function cri_ViewReport( _
                        ByVal RptTitl As String, _
                        ByRef pStim As Date, _
                        ByRef pETim As Date, _
                        ByRef pEnum As Long, _
                        ByRef pEtxt As String, _
                        Optional ByVal ptrName As String, _
                        Optional ByVal ptrDvr As String, _
                        Optional ByVal ptrPort As String, _
                        Optional ByVal ptrOrient As String, _
                        Optional ByVal ptrSource As String) As Long
    
    Dim i                   As Integer
    Dim j                   As Integer
    Dim k                   As Integer
    Dim tDBName             As String
    Dim x                   As Integer
    Dim pIdx                As Integer
    Dim osp                 As Integer
    
    On Error GoTo Crystal_Err
    
    cri_ViewReport = 0
    pEtxt = ""
    pEnum = 0
    
    pStim = Now
    
    'Set report
    crxiReport.ReportTitle = RptTitl
    frmCrystalV.Caption = RptTitl
    crxiReport.EnableParameterPrompting = False
    Load frmCrystalV
    frmCrystalV.rptViewr.ReportSource = crxiReport
    'Pass over printer variables
    If (Not IsMissing(ptrDvr)) And Len(ptrDvr) > 0 Then
        crxiReport.SelectPrinter ptrDvr, ptrName, ptrPort
        crxiReport.PaperOrientation = ptrOrient
        crxiReport.PaperSource = ptrSource
        frmCrystalV.Tag = 1 'use requested printer
    Else
        frmCrystalV.Tag = 0
    End If
    
    frmCrystalV.Show vbModal
    cri_ViewReport = 1
    GoTo Crystal_End
    
Crystal_End:
    On Error Resume Next
    pETim = Now
    Exit Function
    
Crystal_Err:
    pEnum = Err.Number
    pEtxt = Err.Description
    EHandlerADO pEnum, pEtxt

    GoTo Crystal_End
    Exit Function
       
End Function
Public Function cri_PrintReport(ByVal RptTitl As String, _
                        ByRef pStim As Date, _
                        ByRef pETim As Date, _
                        ByRef pEnum As Long, _
                        ByRef pEtxt As String, _
                        Optional ByVal ptrName As String, _
                        Optional ByVal ptrDvr As String, _
                        Optional ByVal ptrPort As String, _
                        Optional ByVal ptrOrient As String, _
                        Optional ByVal ptrSource As String, _
                        Optional ByVal ptrCopies As Long) As Long

    Dim i           As Integer
    Dim osp         As Integer
    Dim c           As Long
    
    On Error GoTo Crystal_Err
    
    cri_PrintReport = 0
    pEtxt = ""
    pEnum = 0
    
    pStim = Now()
    
    crxiReport.ReportTitle = RptTitl
    crxiReport.EnableParameterPrompting = False
    If IsMissing(ptrCopies) Then
        c = 1
    Else
        If IsNull(ptrCopies) Then
            c = 1
        Else
            c = ptrCopies
        End If
    End If
    
    If (Not IsMissing(ptrDvr)) And Len(ptrDvr) > 0 Then
        crxiReport.SelectPrinter ptrDvr, ptrName, ptrPort
        crxiReport.PaperOrientation = ptrOrient
        crxiReport.PaperSource = ptrSource
        frmCrystalV.Tag = 1 'use requested printer
    Else
        frmCrystalV.Tag = 0
    End If

    crxiReport.PrintOut False, c
    cri_PrintReport = 1
    
Crystal_End:
    On Error Resume Next
    pETim = Now
    Exit Function
    
Crystal_Err:
    pEnum = Err.Number
    pEtxt = Err.Description
    EHandlerADO pEnum, pEtxt
    GoTo Crystal_End
    Exit Function
End Function
Public Function cri_SaveReport(ByVal RptTitl As String, _
                        ByRef pStim As Date, _
                        ByRef pETim As Date, _
                        ByRef pEnum As Long, _
                        ByRef pEtxt As String, _
                        ByVal pOutFmt As Long, _
                        ByVal EmailAdd As String, _
                        ByVal MailSubject As String, _
                        ByVal pOutFileName As String) As Long

    Dim i           As Integer
    Dim fIdx        As Long
    Dim osp         As Integer
    
    On Error GoTo CrystalErr
    
    cri_SaveReport = 0
    pEtxt = ""
    pEnum = 0
    
    pStim = Now()
                
    If cri_numFmts < 1 Then LoadOutputFormats
    If pOutFmt > cri_numFmts - 1 Or pOutFmt < 0 Then
        fIdx = cri_defFmtIdx
    Else
        fIdx = pOutFmt
    End If

    crxiReport.ReportTitle = RptTitl
    crxiReport.ExportOptions.FormatType = cri_ofVal(fIdx)
    crxiReport.ExportOptions.DestinationType = crEDTDiskFile
    crxiReport.ExportOptions.DiskFileName = pOutFileName
    crxiReport.Export False
    cri_SaveReport = 1
    
Crystal_End:
    On Error Resume Next
    pETim = Now()
    Exit Function

CrystalErr:
    pEnum = Err.Number
    pEtxt = Err.Description
    EHandlerADO pEnum, pEtxt
    GoTo Crystal_End
    Exit Function
    
End Function
Private Function cri_RptOpen(rptFileName As String, tDBName As String) As Boolean

    Dim i       As Integer
    Dim k       As Integer
    Dim x       As Integer
    Dim fSts    As Boolean
    
    fSts = True
    On Error GoTo CRI_ERR
    
   'Open The report
    Set crxiApplication = New CRAXDRT.Application
    Set crxiReport = New CRAXDRT.Report
    Set crxiReport = crxiApplication.OpenReport(rptFileName, 1)
   
    'Set logon properties for each table within the MAIN report
    For i = 1 To crxiReport.Database.Tables.Count
        crxiReport.Database.Tables(i).SetLogOnInfo _
                    tDBName, tDBName, _
                    gCrystalUser, gCrystalPwd
    Next i
    
    'Set logon properties for each table within the SUBREPORTS
    'Loop through all the objects in the report
    Set crxiSecs = crxiReport.Sections
    For i = 1 To crxiSecs.Count
        Set crxiSec = crxiSecs.Item(i)
        Set crxiRepObjs = crxiSec.ReportObjects
        For x = 1 To crxiRepObjs.Count
            'If the object is a subreport
            If crxiRepObjs.Item(x).Kind = crSubreportObject Then
                Set crxiSubReport = crxiReport.OpenSubreport(crxiRepObjs.Item(x).SubreportName)
                 'Set logon properties for each table
                 'within this SUBREPORT
                For k = 1 To crxiSubReport.Database.Tables.Count
                    crxiSubReport.Database.Tables(k).SetLogOnInfo _
                            tDBName, tDBName, _
                            gCrystalUser, gCrystalPwd
                Next k
            End If
        Next
    Next
    
    GoTo CRI_END
CRI_END:
    cri_RptOpen = fSts
    Exit Function
CRI_ERR:
    fSts = False
    EHandlerADO Err.Number, Err.Description
    GoTo CRI_END
End Function

Private Function cri_SetCriteria( _
                            ByRef SubReportSubName() As String, _
                            ByRef SubReportCriteria() As String, _
                            ByRef subItems As Long, _
                            ByRef SelFormula As String, _
                            ByRef SelGroupFormula As String, _
                            ByRef ParametArry() As String, _
                            ByRef paItems As Long, _
                            ByRef SortArry() As String, _
                            ByRef srtItems As Long) As Boolean
                            
    Dim i                   As Integer
    Dim j                   As Integer
    Dim k                   As Integer
    Dim found               As Boolean
    Dim sortTable           As String
    Dim sortField           As String
    Dim pIdx                As Integer
    Dim fSts                As Boolean
    
    fSts = True
    On Error GoTo CRI_ERR
    
    'Set sub-report selection criteria
    'Format is :    "Name of Subreport"-"selection criteria"
    If UBound(SubReportCriteria()) > 0 Then
        For i = 1 To subItems
            Set crxiSubReport = crxiReport.OpenSubreport(SubReportSubName(i))
            crxiSubReport.RecordSelectionFormula = SubReportCriteria(i)
        Next i
    End If

    'Set record selection criteria
    If Len(Trim(SelFormula)) > 0 Then
        If Trim(crxiReport.RecordSelectionFormula) <> "" And crxiReport.RecordSelectionFormula <> vbCrLf Then
            crxiReport.RecordSelectionFormula = SelFormula
        Else
            crxiReport.RecordSelectionFormula = SelFormula
        End If
    End If
    
    'Set group selection criteria
    If Len(Trim(SelGroupFormula)) > 0 Then
        If Trim(crxiReport.GroupSelectionFormula) <> "" Then
            crxiReport.GroupSelectionFormula = crxiReport.GroupSelectionFormula + " AND " + SelGroupFormula
        Else
            crxiReport.GroupSelectionFormula = SelGroupFormula
        End If
    End If
    
    'Set input parameter fields
    With crxiReport
        If UBound(ParametArry()) > 0 Then
            For i = 1 To paItems Step 2
                found = False
                pIdx = 1
                Do Until found Or pIdx > .ParameterFields.Count
                    If .ParameterFields(pIdx).ParameterFieldName = ParametArry(i) Then
                        found = True
                        Select Case .ParameterFields(pIdx).ValueType
                            Case crBooleanField
                                .ParameterFields(pIdx).AddCurrentValue CBool(ParametArry(i + 1))
                            Case crCurrencyField
                                .ParameterFields(pIdx).AddCurrentValue CCur(ParametArry(i + 1))
                            Case crDateField, crDateTimeField
                                .ParameterFields(pIdx).AddCurrentValue CDate(ParametArry(i + 1))
                            Case crNumberField
                                .ParameterFields(pIdx).AddCurrentValue CInt(ParametArry(i + 1))
                            Case Else
                                .ParameterFields(pIdx).AddCurrentValue CStr(ParametArry(i + 1))
                        End Select
                    Else
                        pIdx = pIdx + 1
                    End If
                Loop
            Next i
        End If
    End With
    
    'Set sort criteria
    If UBound(SortArry()) > 0 Then
        Set crxiTables = crxiReport.Database.Tables
        For i = 1 To srtItems
            sortTable = Left(SortArry(i), InStr(1, SortArry(i), ".") - 1)
            sortField = Mid(SortArry(i), InStr(1, SortArry(i), ".") + 1)
            k = 1
            Do While k <= crxiTables.Count
                If crxiReport.Database.Tables.Item(k).Name = Trim(sortTable) Then
                    j = 1
                    found = False
                    Set crxiDatabaseFieldDefinitions = crxiReport.Database.Tables.Item(k).Fields
                    Do While j <= crxiDatabaseFieldDefinitions.Count
                        If crxiReport.Database.Tables.Item(k).Fields.Item(j).DatabaseFieldName = Trim(sortField) Then
                            crxiReport.RecordSortFields.Add crxiReport.Database.Tables.Item(k).Fields.Item(j), crAscendingOrder
                            found = True
                            Exit Do
                        End If
                        j = j + 1
                    Loop
                End If
                If found = True Then
                    Exit Do
                Else
                    k = k + 1
                End If
            Loop
        Next i
    End If
    
CRI_END:
    cri_SetCriteria = fSts
    Exit Function
CRI_ERR:
    fSts = False
    EHandlerADO Err.Number, Err.Description
    GoTo CRI_END
End Function
Public Sub LoadOutputFormats()
    Dim i   As Long
    
    cri_numFmts = 0
    cri_defFmtIdx = 0
    
    Add2OutFmt "crEFTCommaSeparatedValues", ".CSV - Comma separated values", ".csv", crEFTCommaSeparatedValues
    Add2OutFmt "crEFTCrystalReport", ".RPT - Crystal Report", ".rpt", crEFTCrystalReport
    Add2OutFmt "crEFTDataInterchange", ".DIF - Data Interchange Format", ".dif", crEFTDataInterchange
    Add2OutFmt "crEFTExactRichText", ".RTF - Rich Text Format", ".rtf", crEFTExactRichText
    Add2OutFmt "crEFTExcel80", ".XLS- Excel", ".xls", crEFTExcel80
    cri_defFmtIdx = cri_numFmts  'Put this before the default item
    Add2OutFmt "crEFTPortableDocFormat", ".PDF - Portable Document Format", ".pdf", crEFTPortableDocFormat
    Add2OutFmt "crEFTTabSeparatedValues", ".TXT - TAB Separated values", ".txt", crEFTTabSeparatedValues
    Add2OutFmt "crEFTText", ".TXT - Plain text", ".txt", crEFTText
    Add2OutFmt "crEFTWordForWindows", ".DOC - Microsoft Word Format", ".doc", crEFTWordForWindows
    
End Sub
Private Sub Add2OutFmt( _
                    conNm As String, _
                    conDesc As String, _
                    conExt As String, _
                    conVal As Long)
                    
    cri_ofVal(cri_numFmts) = conVal
    cri_ofDesc(cri_numFmts) = conDesc
    cri_ofExt(cri_numFmts) = conExt
    cri_ofConst(cri_numFmts) = conNm
    cri_numFmts = cri_numFmts + 1
    
End Sub
Public Sub PrintOutDocument( _
                            strDocPath As String, _
                            numCopies As Long, _
                            eNm As Long, _
                            eTxt As String)

    Dim i                   As Long
    Dim f1                  As String
    Dim f2                  As String
    Dim f3                  As String
    Dim Scr_hDC             As Integer
    Const SE_ERR_FNF = 2&
    Const SE_ERR_PNF = 3&
    Const SE_ERR_ACCESSDENIED = 5&
    Const SE_ERR_OOM = 8&
    Const SE_ERR_DLLNOTFOUND = 32&
    Const SE_ERR_SHARE = 26&
    Const SE_ERR_ASSOCINCOMPLETE = 27&
    Const SE_ERR_DDETIMEOUT = 28&
    Const SE_ERR_DDEFAIL = 29&
    Const SE_ERR_DDEBUSY = 30&
    Const SE_ERR_NOASSOC = 31&
    Const ERROR_BAD_FORMAT = 11&
    
    On Error GoTo POD_ERR
    
    eNm = 0
    eTxt = ""
    Scr_hDC = GetDesktopWindow()
    
    UnpackFilePath strDocPath, f1, f2, f3
    For i = 1 To numCopies
        eNm = ShellExecute(Scr_hDC, "Print", f1 & "." & f3, "", f2, 0)
    Next i
    If eNm < 33 And eNm > 0 Then
        Select Case eNm
                Case SE_ERR_FNF
                    eTxt = "File not found"
                Case SE_ERR_PNF
                    eTxt = "Path not found"
                Case SE_ERR_ACCESSDENIED
                    eTxt = "Access denied"
                Case SE_ERR_OOM
                    eTxt = "Out of memory"
                Case SE_ERR_DLLNOTFOUND
                    eTxt = "DLL not found"
                Case SE_ERR_SHARE
                    eTxt = "A sharing violation occurred"
                Case SE_ERR_ASSOCINCOMPLETE
                    eTxt = "Incomplete or invalid file association"
                Case SE_ERR_DDETIMEOUT
                    eTxt = "DDE Time out"
                Case SE_ERR_DDEFAIL
                    eTxt = "DDE transaction failed"
                Case SE_ERR_DDEBUSY
                    eTxt = "DDE busy"
                Case SE_ERR_NOASSOC
                    eTxt = "No association for file extension"
                Case ERROR_BAD_FORMAT
                    eTxt = "Invalid EXE file or error in EXE image"
                Case Else
                    eTxt = "Unknown error"
        End Select
    End If
    
POD_END:
    Exit Sub
POD_ERR:
    eNm = Err.Number
    eTxt = Err.Description
    EHandlerADO Err.Number, Err.Description
    Resume Next
    GoTo POD_END
End Sub
Private Sub PrintTextDocument( _
                            strDocName As String, _
                            numCopies As Long, _
                            numPages As Long, _
                            numLines As Long)
    
    Dim ichan           As Long
    Dim iFileOpen       As Boolean
    Dim fileLen         As Long
    
    Dim orec            As String
    Dim opos            As Long
    Dim numOut          As Long
    Dim oneB            As String * 1
    Dim numRecs         As Long
    Dim numPag          As Long
    
    Dim i               As Long
    Dim j               As Long
    
    Dim osp             As Integer
    
'=====================================================================

    iFileOpen = False
    numPages = 0
    numLines = 0
    
    fileLen = GetFileSize(strDocName)
    If fileLen < 1 Then
        GoTo PrintItButton_End
    End If
        
    On Error GoTo PrintItButton_Err
        
    For i = 1 To numCopies
        ichan = FreeFile()
        Open strDocName For Input As #ichan
        iFileOpen = True
        j = 1
        opos = 0
        orec = ""
        numRecs = 0
        numPag = 0
        Do While j <= fileLen
            oneB = ""
            oneB = Input(1, #ichan)
            Select Case oneB
                Case Chr(13)               'Carriage return
                    Printer.Print orec
                    orec = ""
                    opos = 0
                    numOut = numOut + 1
                Case Chr(14)               'New page
                    If opos > 0 Then
                        Printer.Print orec
                        orec = ""
                        opos = 0
                        numOut = numOut + 1
                    End If
                    Printer.NewPage
                    numPag = numPag + 1
                Case Chr(10)               'Line feed
                Case Else
                    opos = opos + 1
                    orec = orec & oneB
            End Select
            j = j + 1
        Loop
        If opos > 0 Then Printer.Print orec
        Printer.EndDoc
    Next i
    
    numPages = numPag + 1        'First page is zero - so add 1
    numLines = numOut
    GoTo PrintItButton_End

PrintItButton_End:
    On Error Resume Next
    If iFileOpen Then
        iFileOpen = False
        Close #ichan
    End If
    Exit Sub
PrintItButton_Err:
    EHandlerADO Err.Number, Err.Description
    GoTo PrintItButton_End
End Sub
Private Function PrintATRec(fChan As Long, orec As String, olen As Long) As Long
    On Error Resume Next
    Print #fChan, Trim(orec)
    PrintATRec = Err.Number
End Function
Public Function GetFileSize(anyFileName As String) As Long

    Dim ichan           As Long
    Dim fileLen         As Long
    Dim iFileOpen       As Boolean
    
    On Error GoTo GetFileSize_Err
    
    iFileOpen = False
    fileLen = 0
    If anyFileName = "" Then GoTo GetFileSize_End
    
    ichan = FreeFile()
    Open anyFileName For Input As #ichan
    iFileOpen = True
    
    fileLen = LOF(ichan)
    GoTo GetFileSize_End
    
GetFileSize_End:
    On Error Resume Next
    If iFileOpen Then
        iFileOpen = False
        Close #ichan
    End If
    GetFileSize = fileLen
    Exit Function
GetFileSize_Err:
    EHandlerADO Err.Number, Err.Description
    GoTo GetFileSize_End
End Function

Private Sub GetPrinterDriverPort(ptrNam As String, _
                dvrNam As String, porNam As String)

    Dim pIdx            As Integer
    Dim x               As String
    
    dvrNam = ""
    porNam = ""
    pIdx = MatchPrinterName(ptrNam)
    If pIdx > -1 Then
        dvrNam = Printers(pIdx).DriverName
        porNam = Printers(pIdx).Port
    End If
    
End Sub
