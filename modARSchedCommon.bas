Attribute VB_Name = "modARSchedCommon"
Option Explicit
Option Base 0

Public Sub WriteToJobHistory( _
                jbID As Long, runMsg As String, _
                runSecs As Long, rStat As Long)

    Dim rsSQL           As String
    Dim osp             As Integer
    Dim n               As String
    
    On Error GoTo WJH_ERR
    
    n = Format(Now(), "yyyy-mm-dd hh:mm:ss")
    
    rsSQL = "insert into auto_report_run_hist " & _
        "(arjob_id, arhst_run_date, arhst_run_msg, " & _
        "arhst_run_time_sec, arrunstat_id)" & _
        "values (" & jbID & ", getdate(), " & _
        "'" & runMsg & "'," & runSecs & "," & rStat & ")"
    
    gADOconn.Execute rsSQL
    
WJH_END:
    On Error Resume Next
    Exit Sub
WJH_ERR:
    EHandlerADO Err.Number, Err.Description
    GoTo WJH_END
End Sub
