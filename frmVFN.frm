VERSION 5.00
Begin VB.Form frmVFN 
   Caption         =   "ILLEGAL File Name"
   ClientHeight    =   4695
   ClientLeft      =   210
   ClientTop       =   780
   ClientWidth     =   5910
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   ScaleHeight     =   4695
   ScaleWidth      =   5910
   Begin VB.Frame frameHelp 
      Height          =   4455
      Left            =   120
      TabIndex        =   9
      Top             =   120
      Visible         =   0   'False
      Width           =   5655
      Begin VB.TextBox Text1 
         Height          =   3615
         Left            =   120
         MaxLength       =   32768
         MultiLine       =   -1  'True
         TabIndex        =   11
         Top             =   240
         Width           =   5415
      End
      Begin VB.CommandButton cmdHelpClose 
         Caption         =   "Close"
         Height          =   375
         Left            =   4800
         TabIndex        =   10
         Top             =   3960
         Width           =   735
      End
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   2640
      TabIndex        =   3
      Top             =   4200
      Width           =   975
   End
   Begin VB.Frame frameOrig 
      Caption         =   "Original File Name"
      Height          =   1455
      Left            =   120
      TabIndex        =   7
      Top             =   120
      Width           =   5655
      Begin VB.TextBox txtOrig 
         Height          =   1095
         Left            =   120
         Locked          =   -1  'True
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   240
         Width           =   5415
      End
   End
   Begin VB.Frame frameErrors 
      Caption         =   "Errors"
      Height          =   855
      Left            =   120
      TabIndex        =   6
      Top             =   1680
      Width           =   5655
      Begin VB.ListBox listErrors 
         Height          =   450
         Left            =   120
         TabIndex        =   0
         Top             =   240
         Width           =   5415
      End
   End
   Begin VB.Frame frameNew 
      Caption         =   "New File Name"
      Height          =   1455
      Left            =   120
      TabIndex        =   4
      Top             =   2640
      Width           =   5655
      Begin VB.TextBox txtNew 
         Height          =   1095
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   5415
      End
   End
   Begin VB.CommandButton cmdAuto 
      Caption         =   "Auto-Convert"
      Height          =   375
      Left            =   240
      TabIndex        =   2
      Top             =   4200
      Width           =   1335
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   375
      Left            =   4560
      TabIndex        =   5
      Top             =   4200
      Width           =   1095
   End
   Begin VB.Menu Help 
      Caption         =   "Help"
      Begin VB.Menu menHelpAbout 
         Caption         =   "About File Names"
      End
   End
End
Attribute VB_Name = "frmVFN"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdAuto_Click()
    txtNew.Text = CorrectFileName(txtOrig.Text)
End Sub
Private Sub cmdCancel_Click()
    GnewFName = GoldFName
    Unload Me
End Sub
Private Sub cmdClose_Click()
    Dim b       As Boolean
    If VerifyFileName(txtNew.Text, True) Then
        GnewFName = txtNew.Text
        Unload Me
'    Else
'        MsgBox "File name still has errors", _
'                vbExclamation, "Cannot Exit"
    End If
End Sub

Private Sub cmdHelpClose_Click()
    Me.frameHelp.Visible = False
End Sub
Private Sub Form_Load()
    Dim b       As Boolean
    Me.txtOrig = GoldFName
    Me.txtNew = GnewFName
    b = VerifyFileName(txtOrig.Text, True)
    If GcancEnabled Then
        Me.cmdCancel.Enabled = True
    Else
        Me.cmdCancel.Enabled = False
    End If
    LoadHelpText
End Sub
Private Sub LoadHelpText()

    Me.Text1 = _
    "Long Filenames and the Protected-Mode FAT File System" & _
    "=====================================================" & _
    vbCrLf & vbCrLf & _
    "The protected-mode FAT file system is the default file " & vbCrLf & _
    "system used by Windows 95/98/NT for mass storage devices, " & vbCrLf & _
    " such as hard disk and floppy disk drives. " & vbCrLf & _
    "Protected-mode FAT is compatible with the MS-DOS FAT " & vbCrLf & _
    "file system, using file allocation tables and directory " & vbCrLf & _
    "entries to store information about the contents of a " & vbCrLf & _
    "disk drive. Protected-mode FAT also supports long " & vbCrLf & _
    "filenames, storing these names as well as the date " & vbCrLf & _
    "as the date and time that the file was created and the " & vbCrLf & _
    "date that the file was last accessed in the FAT file " & vbCrLf & _
    "system structures." & vbCrLf & vbCrLf
    
    Me.Text1 = Me.Text1 & _
    "The protected-mode FAT file system allows filenames of " & vbCrLf & _
    "up to 256 characters, including the terminating null " & vbCrLf & _
    "character. In this regard, it is similar to the " & vbCrLf & _
    "Microsoft� Windows NT� file system (NTFS), which allows " & vbCrLf & _
    "filenames of up to 256 characters. Protected-mode FAT allows " & vbCrLf & _
    "directory paths (excluding the filename) of up to 246 " & vbCrLf & _
    "characters, including the drive letter, colon, and leading " & vbCrLf & _
    "backslash. "
    
    Me.Text1 = Me.Text1 & _
    "This limit of 246 allows for the addition of a filename in " & vbCrLf & _
    "the standard 8.3 format with the terminating null character. " & vbCrLf & _
    "The maximum number of characters in a full path, including " & vbCrLf & _
    "the drive letter, colon, leading backslash, filename, and " & vbCrLf & _
    "terminating null character, is 260." & vbCrLf & _
vbCrLf

    Me.Text1 = Me.Text1 & _
    "When an application creates a file or directory that has a " & vbCrLf & _
    "long filename, the system automatically generates a " & vbCrLf & _
    "corresponding alias for that file or directory using the " & vbCrLf & _
    "standard 8.3 format. The characters used in the alias are " & vbCrLf & _
    "the same characters that are available for use in MS-DOS file " & vbCrLf & _
    "and directory names. Valid characters for the alias are any " & vbCrLf & _
    "combination of letters, digits, or characters with ASCII " & vbCrLf & _
    "codes greater than 127, the space character (ASCII 20h), as " & vbCrLf & _
    "well as any of the following special characters." & vbCrLf & _
vbCrLf & _
    "$ % ' - _ @ ~ ` ! ( ) { } ^ # &" & vbCrLf & vbCrLf & _
vbCrLf
    
    Me.Text1 = Me.Text1 & _
    "The space character has been available to applications for " & vbCrLf & _
    "filenames and directory names through the functions in " & vbCrLf & _
    "current and earlier versions of MS-DOS. However, many " & vbCrLf & _
    "applications do not recognize the space character as a valid " & vbCrLf & _
    "character, and the system does not use the space character " & vbCrLf & _
    "when it generates an alias for a long filename. " & vbCrLf & _
    "MS-DOS does not distinguish between uppercase and lowercase " & vbCrLf & _
    "letters in filenames and directory names, and this is also " & vbCrLf & _
    "true for aliases."
    
    Me.Text1 = Me.Text1 & _
    "The set of valid characters for long filenames includes all " & vbCrLf & _
    "the characters that are valid for an alias as well as the " & vbCrLf & _
    "following additional characters." & vbCrLf & _
    vbCrLf & _
    "+ , ; = [ ]" & vbCrLf & vbCrLf
    
    Me.Text1 = Me.Text1 & _
    "Windows 95/98/NT preserves the case of the letters used " & vbCrLf & _
    "in long filenames. However, the protected-mode FAT file " & vbCrLf & _
    "system, which is not case sensitive, will not allow more " & vbCrLf & _
    "than one file to have the same name except for case in the " & vbCrLf & _
    "same directory. For example, files named Long File Name and " & vbCrLf & _
    "long file name are not allowed to exist in the same directory. " & vbCrLf & _
    "Although extended ASCII characters (characters with ASCII " & vbCrLf & _
    "codes greater than 127) are also permitted in filenames, " & vbCrLf & _
    "programs should avoid them, because the meanings of the " & vbCrLf & _
    "extended characters may vary according to code page. " & _
vbCrLf
    
    Me.Text1 = Me.Text1 & _
    "On disk, the characters in the alias are stored using " & vbCrLf & _
    "the OEM character set of the current code page, and the " & vbCrLf & _
    "long filename is stored using Unicode format." & vbCrLf & _
    "Although the protected-mode FAT file system is the " & vbCrLf & _
    "default file system in Windows 95/98/NT, it is not the " & vbCrLf & _
    "only file system accessible to applications running with " & vbCrLf & _
    "Windows 95/98/NT."

End Sub
Private Sub menHelpAbout_Click()
    Me.frameHelp.Visible = True
End Sub
