Attribute VB_Name = "modCrystalInterface"
Option Explicit

Global gConf                    As CDO.Configuration
Global gFields                  As ADODB.Fields
Global gMAPI_Signed_ON          As Boolean

Global gRunning_As_A_SERVICE    As Boolean

Global gDefID                   As Long
Global gDataSource              As String   'ODBC connection
Global gUserID                  As String   'User name
Global gPwd                     As String   'Log password
Global gCycleTime               As Long     'Cycle time in second
Global gCrystalDB               As String
Global gCrystalUser             As String
Global gCrystalPwd              As String
Global gReportPath              As String
Global gCommandLine             As String   'Start up command line
Global gIniFile                 As String   'path to .ini file
Global gJobSchedMode             As Boolean  'Run interactive screen displays
Global gLogonUser               As String   'Logged on as userid
Global gSMTPport                As String
Global gSMTPserver              As String
Global gSMTPfrom                As String

Global GV_EMailSessionID        As Long

Global gTimerID            As Long
Global gTimerRunning       As Boolean

Enum storedProcedureType
    procedureStored = 1
    procedureNotStored = 2
End Enum
Dim storedProcedureType      As Long

Private Declare Function Get_TickCount _
                Lib "kernel32.dll" _
                Alias "GetTickCount" () As Long
Private Declare Function GetSignOnName _
            Lib "advapi32.dll" _
            Alias "GetUserNameA" _
            (ByVal lpBuffer As String, nSize As Long) As Long
'
'
'=========================================================

Public Function GetNTLogonName() As String

    Dim sBuffer     As String
    Dim lSize       As Long

    sBuffer = Space$(255)
    lSize = Len(sBuffer)
    
    Call GetSignOnName(sBuffer, lSize)
    If lSize > 0 Then
        If Asc(Mid$(sBuffer, lSize)) = 0 Then lSize = lSize - 1
        GetNTLogonName = Left$(sBuffer, lSize)
    Else
        GetNTLogonName = vbNullString
    End If
    
End Function
Public Function GetTickCount() As Long
    GetTickCount = Get_TickCount()
End Function
Public Function Time2Secs(inTim As Date) As Long
    Dim h       As Long
    Dim m       As Long
    Dim s       As Long
    Dim t       As Long
    On Error GoTo TTS_END
    t = 0
    h = DatePart("h", inTim)
    m = DatePart("n", inTim)
    s = DatePart("s", inTim)
    t = s + (m + h * 60) * 60
TTS_END:
    Time2Secs = t
    Exit Function
End Function
Public Function SecsToHMSstring(inSec As Long) As String

    Dim h       As Long
    Dim m       As Long
    Dim s       As Long
    Dim t       As Long
    Dim x       As String
    
    x = ""
    h = Fix(inSec / 3600)
    t = inSec - h * 3600
    m = Fix(t / 60)
    s = t - m * 60
    If h > 0 Then
        x = h & " hr "
        If m = 0 Then x = x & "0 min "
    End If
    If m > 0 Then
        x = x & m & " min "
    End If
    x = x & s & " sec"
    
    
S2S_END:
    SecsToHMSstring = x
    Exit Function
End Function
Public Sub EMailSignON()
    
    Dim mTxt            As String
    Dim mResp           As Integer
    Dim prfName         As String
    
    On Error GoTo EM_ERR
    
    gMAPI_Signed_ON = False
    With frmMAPI.MAPISession1
        .NewSession = True
        .DownLoadMail = False
        .UserName = "MS Exchange Settings"
        .SignOn
    End With
    
    gMAPI_Signed_ON = True
    
EM_END:
    Exit Sub
EM_ERR:
    EHandlerADO Err.Number, Err.Description, "MAPI Error signing on."
    GoTo EM_END
    
End Sub
Public Sub EMailSignOFF()
    
    On Error GoTo EM_ERR
    GV_EMailSessionID = 0
    If gMAPI_Signed_ON Then
        frmMAPI.MAPISession1.SignOff
    End If
EM_END:
    Exit Sub
EM_ERR:
    GoTo EM_END
End Sub
Public Sub EmailFile( _
                    strSMTPPort As String, _
                    strSMTPServer As String, _
                    strFrom As String, _
                    strSendTo As String, _
                    strBccTo As String, _
                    strSubject As String, _
                    strMessage As String, _
                    strAttachment() As String, _
                    eNm As Long, _
                    eTxt As String)


    Dim x                       As String
    Dim i                       As Long
    Dim mTxt                    As String
    
    Dim iMsg                    As New CDO.Message
    Dim iConf                   As New CDO.Configuration
    Dim iRecip
    Dim Flds                    As ADODB.Fields
    
    On Error GoTo EMF_ERR
    
    eNm = 0
    eTxt = ""
    
    If Len(strFrom) < 1 Then GoTo EMF_END
    If Len(strSendTo) < 1 Then GoTo EMF_END
    If Len(strSubject) < 1 Then GoTo EMF_END
    
    Set Flds = iConf.Fields
    With Flds
        .Item(cdoSendUsingMethod) = cdoSendUsingPort
        .Item(cdoSMTPServer) = strSMTPServer
        .Item(cdoSMTPServerPort) = strSMTPPort
        .Item(cdoSMTPAuthenticate) = cdoAnonymous
        .Update
    End With
        
    With iMsg
        Set .Configuration = iConf
        .To = strSendTo
        .From = strFrom
        .Subject = strSubject
        If Len(Trim(strBccTo)) > 0 Then
            .BCC = Trim(strBccTo)
        End If
        'Loop through and add attachments to email
        For i = LBound(strAttachment) To UBound(strAttachment)
            If Len(Trim(strAttachment(i))) > 0 Then
                .AddAttachment Trim(strAttachment(i))
            End If
        Next i
        If Len(strMessage) > 0 Then
            .TextBody = strMessage
        Else
            .TextBody = "Please see attached." & vbCrLf & vbCrLf & _
                        "This is an automated email and we are unable to respond to replies.  If you require further assistance please contact us by emailing help.reports@aptouring.com.au and quote the Job number."
        End If
        .Send
    End With
    
    eNm = 0
    
EMF_END:
    On Error Resume Next
    Exit Sub
EMF_ERR:
    eNm = Err.Number
    eTxt = Err.Description
    EHandlerADO Err.Number, Err.Description
    GoTo EMF_END
End Sub
'Public Sub SendFax( _
'                        strTo As String, _
'                        strFileName As String, _
'                        strFolderName As String, _
'                        strSubject As String, _
'                        strMsgText As String, _
'                        eNm As Long, _
'                        eTxt As String)
'
'    Dim fxTo                As String
'    Dim mTxt                As String
'    Dim objRecip            As Object
'    Dim attach              As Object
'    On Error GoTo SF_ERR
'    eNm = 0
'    eTxt = ""
'
'    If Len(strMsgText) > 0 Then
'        mTxt = strMsgText
'    Else
'        mTxt = "Please see attached."
'    End If
'
'    fxTo = strTo
'    If Left$(fxTo, 5) <> "[FAX:" Then
'        fxTo = "[FAX:" & fxTo
'    End If
'    If Right$(fxTo, 1) <> "]" Then
'        fxTo = fxTo & "]"
'    End If
''    Set gMAPIMess = CreateObject("MAPI.Message")
'    With frmMAPI.MAPIMessages1
'        .SessionID = frmMAPI.MAPISession1.SessionID
'        .Compose
'        .AddressResolveUI = True
'        .MsgSubject = strSubject
'        .MsgNoteText = mTxt
''        .AttachmentPosition = Len(mTxt) + 1
'        .AttachmentPathName = strFolderName & "\" & strFileName
'        .AttachmentName = strFileName
'        .AttachmentType = 0
'        .RecipAddress = fxTo
'        .RecipDisplayName = fxTo
'        .RecipType = 1
'        .ResolveName
'        .Send False
'    End With
'
''    Set gMAPIMess = New CDO.Message
''    With gMAPIMess
''        Set .Configuration = gConf
''        .To = fxTo
''        .From = gSMTPfrom
''
''        .Subject = strSubject
''        .AddAttachment strFolderName & "\" & strFileName
''        .Send
''    End With
'
''    With gMAPIMess
''        .Sender = gSMTPfrom
''        .Subject = strSubject
''        .TextBody = mTxt
'
''        .AddAttachment strFolderName & "\" & strFileName
''        Set attach = .Attachments.Add
''        attach.AttachmentName = strFileName
''        attach.AttachmentPathName = strFolderName & "\" & strFileName
''        .To = fxTo
''        Set objRecip = .Recipients.Add
''        objRecip.Name = fxTo
''        objRecip.Type = 1 ' 1 is the value of the "mapiTo" constant.
''        objRecip.Resolve
''        .Update
''        .Send
''    End With
'
'SF_END:
'    On Error Resume Next
'    Exit Sub
'SF_ERR:
'    eNm = Err.Number
'    eTxt = Err.Description
'    EHandlerADO Err.Number, Err.Description
'    Resume Next
'    GoTo SF_END
'End Sub

Public Sub SendFax( _
                        strTo As String, _
                        strFileName As String, _
                        strFolderName As String, _
                        strSubject As String, _
                        strMsgText As String, _
                        eNm As Long, _
                        eTxt As String)

    Dim fxTo                As String
    Dim mTxt                As String
    Dim objRecip            As Object
    Dim attach              As Object
    On Error GoTo SF_ERR
    eNm = 0
    eTxt = ""

    If Fax_Method = fax_with_FaxWare Then GoTo SendFax_FaxWare

SendFax_FaxMaker:
    fxTo = "fax.maker@aptouring.com.au"
    mTxt = "::high" & _
        vbCrLf & "::c=APTCoverBrief" & _
        vbCrLf & "::" & strTo & vbCrLf
        'KC 14 Jul 2005
        'remove these lines from between above 2 lines.
        'vbCrLf & "::p=high" & _
        'vbCrLf & "::c=none"
    If Len(strMsgText) > 0 Then mTxt = mTxt & vbCrLf & strMsgText
    GoTo SendFax_SendIT
    
SendFax_FaxWare:
    If Len(strMsgText) > 0 Then
        mTxt = strMsgText
    Else
        mTxt = "Please see attached."
    End If

    fxTo = strTo
    If Left$(fxTo, 5) <> "[FAX:" Then
        fxTo = "[FAX:" & fxTo
    End If
    If Right$(fxTo, 1) <> "]" Then
        fxTo = fxTo & "]"
    End If
    GoTo SendFax_SendIT
    
SendFax_SendIT:
    With frmMAPI.MAPIMessages1
        .SessionID = frmMAPI.MAPISession1.SessionID
        .Compose
        .AddressResolveUI = True
        .MsgSubject = strSubject
        .MsgNoteText = mTxt
        If Fax_Method = fax_with_FaxMaker Then
            .AttachmentPosition = Len(mTxt) - 1
        End If
        .AttachmentPathName = strFolderName & "\" & strFileName
        .AttachmentName = strFileName
        .AttachmentType = 0
        .AttachmentIndex = 0
        .RecipAddress = fxTo
        .RecipDisplayName = fxTo
        .RecipType = 1
        .ResolveName
        .Send False
    End With
        
SF_END:
    On Error Resume Next
    Exit Sub
SF_ERR:
    eNm = Err.Number
    eTxt = Err.Description
    EHandlerADO Err.Number, Err.Description
    GoTo SF_END
End Sub

Public Function SetSQLStoredProc( _
                procName As String, _
                cn As ADODB.Connection, _
                ParamArray parArgs() As Variant) As ADODB.Command
    
    Dim qp          As ADODB.Parameter
    Dim i           As Integer
    Dim x           As String
    Dim aST         As Integer
    Dim aEN         As Integer
    Dim osp         As Integer
    
    On Error GoTo ESP_ERR
    
    Set SetSQLStoredProc = Nothing
    aST = LBound(parArgs)
    aEN = UBound(parArgs)
    x = procName
    Set SetSQLStoredProc = New ADODB.Command
    With SetSQLStoredProc
        Set .ActiveConnection = cn
        .CommandType = adCmdStoredProc
        .CommandTimeout = gADOTimeOut
        .CommandText = x
        .Parameters.Refresh
    End With
    For i = aST To aEN Step 2
        If i < aEN Then
            x = UCase(parArgs(i))
            For Each qp In SetSQLStoredProc.Parameters
                If UCase(qp.Name) = x Then
                    qp.Value = parArgs(i + 1)
                    Exit For
                End If
            Next
        End If
    Next i
    
ESP_END:
    On Error Resume Next
    Exit Function
ESP_ERR:
    EHandlerADO Err.Number, Err.Description
    GoTo ESP_END
    
End Function
Public Sub ExtractQryPars( _
                sQrySProcName As String, _
                qrytype As storedProcedureType, _
                lMaxPars As Long, lNumPars As Long, _
                prName() As String, _
                prDType() As Long, _
                prDTypeDesc() As String, _
                prFLen() As Long, _
                Optional optADOConn As ADODB.Connection, _
                Optional optJetDB As String)

    Dim qSQL        As String
    Dim i           As Integer
    Dim db          As ADODB.Connection
    Dim rs          As ADODB.Recordset
    Dim cmd         As ADODB.Command
    Dim cat         As ADOX.Catalog
    Dim rsSQL       As String
    Dim x           As String
    
    lNumPars = 0

    On Error GoTo EQP_ERR
    
    If Len(sQrySProcName) < 1 Then GoTo EQP_END
    If qrytype = procedureNotStored Then GoTo JET_QRYDEF
    
    rsSQL = "Select so.name as SPName, sc.name as ParmName, " & _
        "st.xtype As DataType, st.name as DataTypeDesc, " & _
        "sc.length as FldLength " & _
        "FROM syscolumns sc,master..systypes st,sysobjects so " & _
        "WHERE sc.id in (select id from sysobjects where " & _
        "uid = user_id() and type = 'P' and " & _
        "name ='" & sQrySProcName & "') " & _
        "AND so.type ='P' AND sc.id = so.id AND sc.xtype = st.xtype"
        
    Set rs = New ADODB.Recordset
    rs.Open rsSQL, optADOConn, adOpenKeyset, adLockReadOnly
    With rs
        If Not .EOF Then .MoveFirst
        Do Until .EOF
            prName(lNumPars) = ![ParmName]
            prDType(lNumPars) = ADODtypeToJet(CLng(![DataType]))
            prDTypeDesc(lNumPars) = ![DataTypeDesc]
            prFLen(lNumPars) = ![FldLength]
            lNumPars = lNumPars + 1
            .MoveNext
        Loop
    End With
    GoTo EQP_END
    
JET_QRYDEF:
    Set db = New ADODB.Connection
    qSQL = sQrySProcName
    x = "Provider=Microsoft.Jet.OLEDB.4.0;" & _
        "Data Source=" & optJetDB & ";" & _
        "Persist Security Info=False"
    db.Open x
    Set cat.ActiveConnection = db
    Set cmd = cat.Procedures(qSQL).Command
    With cmd
        .CommandTimeout = gADOTimeOut
        For i = 0 To .Parameters.Count - 1
            prName(lNumPars) = .Parameters(i).Name
            prDType(lNumPars) = .Parameters(i).Type
            prDTypeDesc(lNumPars) = DecodeJetDatatype(prDType(lNumPars))
            prFLen(lNumPars) = 0
            lNumPars = lNumPars + 1
        Next i
    End With
    GoTo EQP_END

EQP_END:
    Set rs = Nothing
    Set cmd = Nothing
    Set cat = Nothing
    Set db = Nothing
    Exit Sub
    
EQP_ERR:
    EHandlerADO Err.Number, Err.Description
    GoTo EQP_END
    
End Sub
Public Function DecodeJetDatatype(dtCode As Long) As String
    Dim x           As String
    x = ""
    Select Case dtCode
        Case 1
            x = "Yes/No"
        Case 2
            x = "Byte"
        Case 3
            x = "Integer"
        Case 4
            x = "Long Integer"
        Case 5
            x = "Currency"
        Case 6
            x = "Single Precision"
        Case 7
            x = "Double Precision"
        Case 8
            x = "Date/Time"
        Case 9
            x = "Binary"
        Case 10
            x = "Text"
    End Select
    DecodeJetDatatype = x
End Function
Public Function DecodeADODatatype(dtCode As Long) As String
    Dim x           As String
    x = ""
    Select Case dtCode
        Case 0: x = "adEmpty"
        Case 16: x = "adTinyInt"
        Case 2: x = "adSmallInt"
        Case 3: x = "adInteger"
        Case 20: x = "adBigInt"
        Case 17: x = "adUnsignedTinyInt"
        Case 18: x = "adUnsignedSmallInt"
        Case 19: x = "adUnsignedInt"
        Case 21: x = "adUnsignedBigInt"
        Case 4: x = "adSingle"
        Case 5: x = "adDouble"
        Case 6: x = "adCurrency"
        Case 14: x = "adDecimal"
        Case 131: x = "adNumeric"
        Case 11: x = "adBoolean"
        Case 10: x = "adError"
        Case 132: x = "adUserDefined"
        Case 12: x = "adVariant"
        Case 9: x = "adIDispatch"
        Case 13: x = "adIUnknown"
        Case 72: x = "adGUID"
        Case 7: x = "adDate"
        Case 133: x = "adDBDate"
        Case 134: x = "adDBTime"
        Case 135: x = "adDBTimeStamp"
        Case 8: x = "adBSTR"
        Case 129: x = "adChar"
        Case 200: x = "adVarChar"
        Case 201: x = "adLongVarChar"
        Case 130: x = "adWChar"
        Case 202: x = "adVarWChar"
        Case 203: x = "adLongVarWChar"
        Case 128: x = "adBinary"
        Case 204: x = "adVarBinary"
        Case 205: x = "adLongVarBinary"
        Case Else:
            x = "Unknown DataTypeEnum of " & _
            dtCode & " found."
    End Select
    DecodeADODatatype = x
End Function
Public Function ADODtypeToJet(adoCode As Long) As Long
    Dim l       As Long
    l = 0
    Select Case adoCode
        Case 173, 165
            l = 9
        Case 104
            l = 1
        Case 175, 239, 99, 231, 35, 167, 231, 34
            l = 10
        Case 61, 58, 189
            l = 8
        Case 106, 62, 59, 108
            l = 7
        Case 56, 36
            l = 4
        Case 60, 122
            l = 5
        Case 52
            l = 3
        Case 48
            l = 2
    End Select
    ADODtypeToJet = l
End Function
Public Sub WriteToEventLog(mTxt As String)
    On Error Resume Next
    App.LogEvent mTxt, vbLogEventTypeError
End Sub
Public Function IsLoaded(txtFormName As String) As Boolean

    Dim f   As Boolean
    Dim i   As Integer
    f = False
    For i = 0 To Forms.Count - 1
        If Forms(i).Name = txtFormName Then
            f = True
            i = Forms.Count + 1
        End If
    Next i
    IsLoaded = f
End Function
Public Sub UnloadFormName(txtFormName As String)
    Dim i           As Integer
    Dim f           As Boolean
    
    f = False
    i = 0
    Do Until f Or (i > Forms.Count - 1)
        If Forms(i).Name = txtFormName Then
            Unload Forms(i)
            f = True
        Else
            i = i + 1
        End If
    Loop
    
End Sub
Public Sub UnpackFilePath( _
                    strFileNameFull As String, _
                    strFileName As String, _
                    strDir As String, _
                    strSuffix As String)
                    
    Dim x       As String
    Dim t       As String
    Dim p       As Long
    Dim l       As Long
    Dim i       As Long
       
    Dim tDirPath        As String
    Dim tFileName       As String
    Dim tSuffix         As String
    Dim tFileNameFull   As String
        
    tDirPath = ""
    tFileName = ""
    tSuffix = ""
    t = Trim$(NullToS(strFileNameFull))
    tFileNameFull = t

    l = Len(t)
    If l < 1 Then GoTo SF_END
    
'Go backwards and look for a "."
    p = l
    i = 0
    Do Until i > 0 Or p < 1
        If Mid$(t, p, 1) = "." Then
            i = p
        Else
            p = p - 1
        End If
    Loop
    If i <> l And i <> 0 Then tSuffix = Right$(t, l - i)
    l = i - 1
    If l > 0 Then t = Left$(t, l)
    
'Go backwards look for a "\"
    l = Len(t)
    p = l
    i = 0
    Do Until i > 0 Or p < 1
        If Mid$(t, p, 1) = "\" Then
            i = p
        Else
            p = p - 1
        End If
    Loop
    If i <> l Then tFileName = Right$(t, l - i)
    l = i - 1
    If l > 0 Then tDirPath = Left$(t, l)
        
SF_END:
    strDir = tDirPath
    strSuffix = tSuffix
    strFileName = tFileName
    Exit Sub
    
End Sub
Public Function UnStringCommandLine(cmdLine As String, _
                            maxArgs As Long, _
                            argArray() As Variant) As Long

    Dim cl              As String
    Dim clL             As Long
    Dim i               As Long
    Dim argIDX          As Long
    Dim cIDX            As Long
    Dim c               As String * 1
    Dim qc              As Long
    Dim numArgs         As Long
    Dim dc              As String
    
    UnStringCommandLine = 0
    cl = Replace(cmdLine, "  ", " ")
    clL = Len(cl)
    dc = Chr(34)
    cIDX = 1
    qc = 0
    numArgs = 0
    argArray(1) = ""
    
    Do Until cIDX > clL
        c = Mid$(cl, cIDX, 1)
        If qc > 0 Then
            If c <> dc Then
                argArray(numArgs) = argArray(numArgs) & c
            Else
                qc = 0
            End If
        Else
            qc = 0
            If c = dc Then
                qc = qc + 1
            Else
                If (c = " " Or c = vbTab) Then
                    If numArgs < maxArgs Then
                        numArgs = numArgs + 1
                        argArray(numArgs) = ""
                    Else
                        cIDX = clL + 1   'Exit from loop
                    End If
                Else
                    If numArgs = 0 Then numArgs = 1
                    argArray(numArgs) = argArray(numArgs) & c
                End If
            End If
        End If
        cIDX = cIDX + 1
    Loop
    
    UnStringCommandLine = numArgs
    
End Function
Public Sub PassArgumentFormula( _
                        inF As String, _
                        outF As String, _
                        dtFormat As Long, _
                        rStat As Long)

    Dim i           As Long
    Dim Pos         As Long
    Dim l           As Long
    Dim p           As Long
    Dim nb          As Long
    Dim d           As Date
    Dim x           As String
    Dim r           As String
    Dim t           As String
    Dim f           As Boolean
    
    outF = ""
    f = False
    rStat = 0
    x = Trim$(inF)
    l = Len(x)
    If l < 1 Then GoTo PAF_END
    outF = x
    x = UCase(outF)
    
'   find arg_user()
    p = InStr(1, x, "ARG_USER()")
    Do Until p = 0
        f = True
        l = Len(outF)
        r = Mid$(x, p, 10)
        r = DecodeArgumentFunction(rStat, r)
        If rStat <> 0 Then GoTo PAF_ERR
        x = Left$(outF, p - 1) & "'" & r & "'"
        If l > 10 Then x = x & Right$(outF, l - 10)
        outF = x
        x = UCase(outF)
        p = InStr(1, x, "ARG_USER()")
    Loop
    
'   decode any date functions
    p = InStr(1, x, "ARG_DATE(")
    Do Until p = 0
        f = True
        l = Len(x)
        r = Mid$(x, p, 9)
        nb = 1
        i = p + 9
        Do Until nb = 0 Or i > l
            t = Mid$(x, i, 1)
            If t = ")" Then
                nb = nb - 1
                If nb = 0 Then r = r & t
            Else
                If t = "(" Then
                    nb = nb + 1
                End If
            End If
            If nb <> 0 Then r = r & t
            i = i + 1
        Loop
        t = DecodeArgumentFunction(rStat, r)
        If rStat <> 0 And rStat <> 1 Then GoTo PAF_ERR
        If dtFormat = 0 Then
            x = Left$(outF, p - 1) & "DATE("
            d = CDate(t)
            x = x & Format(d, "yyyy") & "," & _
                Format(d, "mm") & "," & _
                Format(d, "dd") & ")"
        Else
            x = Left$(outF, p - 1) & _
                    Format(CDate(t), "yyyy-mm-dd")
        End If
        i = l - (p + Len(r) - 1)
        If i > 0 Then x = x & Right$(outF, i)
        outF = x
        x = UCase(outF)
        p = InStr(1, x, "ARG_DATE(")
    Loop
    
'   get the rest
'   find arg_user()
    p = InStr(1, x, "ARG_FILE(")
    Do Until p = 0
        f = True
        l = Len(outF)
        r = Mid$(x, p, l - p)
        Pos = InStr(1, r, " ")
        r = Left$(r, Pos - 1)
        r = DecodeArgumentFunction(rStat, r)
        If rStat <> 0 Then GoTo PAF_ERR
        x = Left$(outF, p - 1) & " " & r & " "
        x = x & Right$(outF, l - Pos)
        outF = x
        x = UCase(outF)
        p = InStr(1, x, "ARG_FILE(")
    Loop
PAF_END:
    If rStat = 0 And f Then rStat = 1
    Exit Sub
PAF_ERR:
    GoTo PAF_END
End Sub
Public Function DecodeArgumentFunction( _
                            rStat As Long, _
                            strParValue As String, _
                            Optional optParDate As Variant) As String
    Dim inPar           As String
    Dim inParL          As Long
    Dim oPar            As String
    Dim d               As Date
    Dim i               As Long
    Dim x               As String
    Dim t               As String
    Dim fnm             As String
    Dim fver            As Long
    Dim fldl            As Long
    Dim idx             As Long
    Dim p               As Long
    
    Dim fnObj           As starFNameObj
    
    Dim funcType        As String
    Dim funcIdx         As Long
    
    Dim cmd             As String
    Dim cmdL            As Long
    Dim cmdType         As Long
    
    Dim nd              As Date     'New date
    Dim newPar          As String   'New date string
    Dim daDate          As Date     'DATEADD temps
    Dim daInt           As String
    Dim daAmt           As Double
    Dim ysm             As Long
    Dim y               As Long
    Dim m               As Long
    
    Const maxCmd                As Long = 256
    Dim numCmd                  As Long
    Dim cmdIdx                  As Long
    Dim cmdArg(1 To maxCmd)     As Variant
    
    On Error Resume Next
    If Not IsMissing(optParDate) Then
        If Not IsNull(optParDate) Then d = optParDate
    End If
    newPar = ""
    rStat = 0
    nd = d
    On Error GoTo DDP_END
    
    newPar = Trim$(NullToS(strParValue))
    inPar = UCase(newPar)
    inParL = Len(inPar)
    If inParL < 10 Then GoTo DDP_END
    idx = InStr(1, inPar, "(")
    If idx < 1 Then GoTo DDP_END
    funcType = Left$(inPar, idx - 1)
    funcIdx = -1
    Select Case funcType
        Case "ARG_DATE"
            funcIdx = 0
        Case "ARG_USER"
            funcIdx = 1
        Case "ARG_FILE"
            funcIdx = 2
    End Select
    If funcIdx < 0 Then GoTo DDP_END
    If funcIdx = 1 Then GoTo DDP_USER
    
    If Right$(inPar, 1) <> ")" Then
        rStat = 101 'missing right )
        GoTo DDP_END
    End If
    'strip off
    inPar = Left$(newPar, inParL - 1)
    inParL = inParL - 1
    inPar = Right$(inPar, inParL - 9)
    
    UnstringARGCommand inPar, maxCmd, numCmd, cmdArg
    
    If numCmd < 1 Then
        rStat = 102 'no arguments
        GoTo DDP_END
    End If
    
    If funcIdx = 2 Then GoTo DDP_FILE

    cmd = UCase(cmdArg(1))
    cmdL = Len(cmd)
    cmdType = -1
    
    Select Case cmd
        Case "GETDATE", "GETDATE()"
            cmdType = 0
        Case "DATEADD"
            cmdType = 1
        Case "FDM"  '1st day of month
            cmdType = 2
        Case "LDM"  'Last day of month
            cmdType = 3
        Case "YTDS" 'Year to date start
            cmdType = 4
        Case "YTDE" 'Year to date end
            cmdType = 5
    End Select
    If cmdType < 0 Then
        rStat = 103 'illegal command
        GoTo DDP_END
    End If
  
    Select Case cmdType
        Case 0  'GETDATE - No Args: none
            If numCmd > 1 Then
                rStat = 105 'Too many parameters for ARG_GETDATE function
                GoTo DDP_END
            End If
            nd = Now()
        Case 1  'DATEADD - Args: date,interval,number
            If numCmd <> 4 Then
                rStat = 104 'Icorrect parameters for date add
                GoTo DDP_END
            End If
            x = UCase(cmdArg(2))
            If x = "GETDATE" Or x = "GETDATE()" Then
                daDate = Now
            Else
                If Not IsDate(cmdArg(2)) Then
                    rStat = 106 'Illegal date in argument 2
                    GoTo DDP_END
                Else
                    daDate = CDate(cmdArg(2))
                End If
            End If
            daInt = cmdArg(3)
            daAmt = CDbl(cmdArg(4))
            nd = DateAdd(daInt, daAmt, daDate)
        Case 2, 3 'FDM,LDM 1st or Last day of month
            If numCmd <> 3 Then
                rStat = 109 'Incorrect parameters for FDM/LDM
                GoTo DDP_END
            End If
            x = UCase(cmdArg(2))
            If x = "GETDATE" Or x = "GETDATE()" Then
                daDate = Now
            Else
                If Not IsDate(cmdArg(2)) Then
                    rStat = 106 'Illegal date in argument 2
                    GoTo DDP_END
                Else
                    daDate = CDate(cmdArg(2))
                End If
            End If
            daAmt = CDbl(cmdArg(3))
            daDate = DateAdd("m", daAmt, daDate)
            If cmdType = 2 Then
                daDate = CDate(Format(daDate, "yyyy-mm") & "-01")
            Else
                daDate = DateAdd("m", 1, daDate)
                daDate = CDate(Format(daDate, "yyyy-mm") & "-01")
                daDate = DateAdd("d", -1, daDate)
            End If
            nd = daDate
        Case 4, 5   'Year to date
            If numCmd <> 4 Then
                rStat = 111 'Incorrect parameters for YTDS/YTDE
                GoTo DDP_END
            End If
            If Not IsNumeric(cmdArg(4)) Then
                rStat = 112 'Invalid month
                GoTo DDP_END
            Else
                ysm = CLng(cmdArg(4))
                If ysm < 1 Or ysm > 12 Then
                    rStat = 112
                    GoTo DDP_END
                End If
            End If
            
            x = UCase$(cmdArg(2))
            If x = "GETDATE" Or x = "GETDATE()" Then
                daDate = Now
            Else
                If Not IsDate(cmdArg(2)) Then
                    rStat = 106 'Illegal date in argument 2
                    GoTo DDP_END
                Else
                    daDate = CDate(cmdArg(2))
                End If
            End If
            daAmt = CDbl(cmdArg(3))
            daDate = DateAdd("m", daAmt, daDate)
            m = Month(daDate)
            y = Year(daDate)
            If cmdType = 4 Then
                If m < ysm Then y = y - 1
                daDate = CDate(Format(y, "0000") & _
                        "-" & Format(ysm, "00") & "-01")
            Else
                If ysm > 12 Then
                    ysm = 1
                    y = y + 1
                End If
                If ysm <= m Then y = y + 1
                daDate = CDate(Format(y, "0000") & _
                        "-" & Format(ysm, "00") & "-01")
                daDate = DateAdd("d", -1, daDate)
            End If
            nd = daDate
    End Select
    
    newPar = Format(nd, "dd mmm yyyy")
    rStat = 1 'Success
    GoTo DDP_END
    
DDP_USER:
    If numCmd <> 0 Then
        rStat = 107 'Too many arguments for ARG_USER function
        GoTo DDP_END
    End If
    newPar = GetNTLogonName()
    rStat = 1
    GoTo DDP_END
    
DDP_FILE:
    'Args:
    '1=Folder Name 2=Report 3=JobSeq 4=Suffix

    If numCmd <> 4 Then
        rStat = 108 'Incorrect number of arguments for ARG_FILE function
        GoTo DDP_END
    End If
    'Get inline command args possible only on args 1-3
    'replace each with values
    If IsNumeric(cmdArg(3)) Then
        cmdArg(3) = Format(cmdArg(3), "000000")
    End If
    
    rStat = 1
    
    For i = 1 To 3
        x = UCase(cmdArg(i))
        Select Case x
            Case "GETTIME()"
                If i > 1 Then cmdArg(i) = Format(Format(Now(), "hhmmss"), "000000")
            Case "GETDTIME()"
                If i > 1 Then cmdArg(i) = Format(Now(), "yyyymmdd-hhmmss")
            Case "YYYYMMDD()"
                If i > 1 Then cmdArg(i) = Format(Date, "YYYYMMDD")
            Case "JOBSEQ()"
                If i > 1 Then
                    cmdArg(i) = "ssssss"
                    rStat = 98
                End If
            Case "JOBID()"
                If i > 1 Then
                    cmdArg(i) = "jjjjjj"
                    rStat = 99
                End If
        End Select
    Next i
        
    t = cmdArg(1)
    If Right$(t, 1) <> "\" Then t = t & "\"
    t = t & cmdArg(2)
    t = t & "-" & cmdArg(3)
    t = t & "-"
    fnm = t 'Save this bit
    fver = 0
    fldl = 1
DDP_FINDFILE:
    Do Until fldl = 0 Or fver > 99999
        t = fnm & Format(fver, "00000")
        If Left$(cmdArg(4), 1) <> "." Then t = t & "."
        t = t & cmdArg(4)
        fldl = Len(Dir(t))
        fver = fver + 1
    Loop
    If fver > 99999 Then
        fnm = fnm & "A"
        GoTo DDP_FINDFILE
    End If
    newPar = t
    If rStat = 98 Or rStat = 99 Then
        fnObj = SetFNameObj(newPar)
        If Not VerifyFileName(fnObj.FileName & "." & fnObj.Suffix, True) Then rStat = 110
    End If
    GoTo DDP_END
    
DDP_END:
    DecodeArgumentFunction = newPar
    Exit Function
    
End Function
Public Sub UnstringARGCommand(cmdLine As String, _
                            maxArgs As Long, _
                            numArgs As Long, _
                            argArray() As Variant)

    Dim cl              As String
    Dim clL             As Long
    Dim i               As Long
    Dim argIDX          As Long
    Dim cIDX            As Long
    Dim c               As String * 1
    Dim qc              As Long
    Dim dc              As String
    
    cl = cmdLine
    cl = Trim$(cl)
    clL = Len(cl)
    cIDX = 1
    numArgs = 0
    argArray(1) = ""
    
    Do Until cIDX > clL
        c = Mid$(cl, cIDX, 1)
        If c = "," Then
            If numArgs < maxArgs Then
                numArgs = numArgs + 1
                argArray(numArgs) = ""
            Else
                cIDX = clL + 1   'Exit from loop
            End If
        Else
            If numArgs = 0 Then numArgs = 1
            argArray(numArgs) = argArray(numArgs) & c
        End If
        cIDX = cIDX + 1
    Loop
    
End Sub
Public Function ARGStatMsg(rStat As Long) As String
    Dim rMsg        As String
    ARGStatMsg = ""
    Select Case rStat
        Case 0
            rMsg = "OK, not an ARG function"
        Case 1
            rMsg = "Success"
        Case 98
            rMsg = "Success" & " - " & _
                "ssssss will translate to the sequence number of the scheduled job."
        Case 99
            rMsg = "Success" & " - " & _
                "jjjjjj will translate to the job number of the scheduled job."
        Case 101
            rMsg = "Missing right parenthesis )"
        Case 102
            rMsg = "No arguments supplied"
        Case 103
            rMsg = "Illegal command"
        Case 104
            rMsg = "Incorrect number of parameters for ARG_DATEADD function"
        Case 105
            rMsg = "Too many parameters for ARG_GETDATE function"
        Case 106
            rMsg = "Illegal date in argument 2"
        Case 107
            rMsg = "Too many arguments for ARG_USER function"
        Case 108
            rMsg = "Incorrect number of arguments for ARG_FILE function"
        Case 109
            rMsg = "Incorrect number of arguments for LDM/FDM function"
        Case 110
            rMsg = "Incorrect file name characters exist in the filename"
        Case 111
            rMsg = "Incorrect number of arguments for YTDS/YTDE function"
        Case 112
            rMsg = "Incorrect month argument for YTDS/YTDE function"
    End Select
    ARGStatMsg = rMsg
End Function
Public Function ADODateString(vDate As Date) As String
    Dim td      As String
    td = ""
    On Error Resume Next
    td = "CONVERT(DATETIME, '" & _
        Format(vDate, "yyyy-mm-dd") & _
        " 00:00:00',102)"
    ADODateString = td
End Function
Public Sub LoadIniFile(strIniFileName As String, _
                        maxArgs As Long, _
                        numArgs As Long, _
                        argArray() As Variant)

    Dim f               As Long
    Dim fOpen           As Boolean
    Dim x               As String
    Dim o               As String
    Dim a               As String
    Dim i               As Long
    Dim j               As Long
    Dim s               As String
    
    fOpen = False
    numArgs = 0
    On Error GoTo LIF_ERR
    If CalcLen(strIniFileName) < 1 Then GoTo LIF_END
    f = FreeFile()
    Open strIniFileName For Input As #f
    fOpen = True
    
LIF_READ:
    If EOF(CInt(f)) Then GoTo LIF_OVER
    Input #f, x
    i = InStr(x, "=") - 1
    o = ""
    If i < 1 Then GoTo LIF_READ
    o = UCase(Trim$(Left$(x, i)))
    o = Replace(o, vbTab, "")
    i = InStr(x, "=")
    If i < 1 Then GoTo LIF_READ
    j = Len(x)
    a = Right$(x, j - i)
    s = ""
    Select Case o
        Case "DATASOURCE"
            s = "-D"
        Case "USER"
            s = "-U"
        Case "PASSWORD"
            s = "-P"
        Case "DEFAULT_SETTINGS"
            s = "-DEF"
        Case "INTERACTIVE_CYCLE"
            s = "-CI"
        Case "CRYSTAL_USER"
            s = "-CU"
        Case "CRYSTAL_PASSWORD"
            s = "-CP"
        Case "REPORT_PATH"
            s = "-RP"
        Case "REPORT_DATASOURCE"
            s = "-RD"
        Case "MONITOR_MODE"
            s = "-MM"
        Case "START_AS_SERVICE"
            s = "-SVC"
    End Select
    
    If Len(s) < 1 Then GoTo LIF_READ
    j = 0
    For i = 1 To numArgs Step 2
        If argArray(i) = s Then j = i
    Next i
    If j < 1 Then
        i = numArgs + 1
        If numArgs > maxArgs - 1 Then GoTo LIF_READ
        numArgs = numArgs + 2
    Else
        i = j
    End If
    argArray(i) = s
    argArray(i + 1) = Trim$(a)
    
    GoTo LIF_READ
    
LIF_OVER:
    GoTo LIF_END
    
LIF_END:
    On Error Resume Next
    If fOpen Then
        fOpen = False
        Close #f
    End If
    Exit Sub
    
LIF_ERR:
    EHandlerADO Err.Number, Err.Description, "Error loading ini file: " & strIniFileName
    GoTo LIF_END
End Sub
Public Sub LoadDefaultSettings(dsID As Long)

    Dim rs          As ADODB.Recordset
    Dim rsOpen      As Boolean
    Dim rsSQL       As String
    Dim osp         As Integer
    
    rsOpen = False
    On Error GoTo LDS_ERR
    
    rsSQL = "select * from auto_report_defaults " & _
                "where ardef_id=" & dsID
    Set rs = New ADODB.Recordset
    rs.Open rsSQL, gADOconn, adOpenForwardOnly, adLockReadOnly
    rsOpen = True
    With rs
        If .EOF Then GoTo LDS_END
        
        gCycleTime = NullToZero(![ardef_submit_cycle])
        gCrystalDB = NullToS(![ardef_report_datasource])
        gCrystalUser = NullToS(![ardef_crystal_userid])
        gCrystalPwd = NullToS(![ardef_crystal_pwd])
        gReportPath = NullToS(![ardef_report_path])
        gSMTPport = NullToS(![ardef_smtp_port])
        gSMTPserver = NullToS(![ardef_smtp_server])
        gSMTPfrom = NullToS(![ardef_smtp_from])
    End With
    
LDS_END:
    On Error Resume Next
    If rsOpen Then
        rsOpen = False
        rs.Close
    End If
    Set rs = Nothing
    Exit Sub
LDS_ERR:
    EHandlerADO Err.Number, Err.Description
    GoTo LDS_END
End Sub
Public Sub SetADOConn( _
                        cn As ADODB.Connection, _
                        dsn As String, _
                        uID As String, _
                        pwd As String, _
                        openFlag As Boolean)
    
    Dim cs              As String
    
    On Error GoTo SG_ERR
    
    openFlag = False
    Set cn = Nothing
    Set cn = New ADODB.Connection
    cn.CursorLocation = adUseClient
    If CalcLen(uID) < 1 Then
        cs = "PROVIDER=MSDASQL;" & "DSN=" & dsn & ";UID=;PWD=;"
    Else
        cs = "DSN=" & dsn & ";UID=" & uID & ";PWD=" & pwd & ";"
    End If
    
    cn.Open cs
    openFlag = True
    
SG_END:
    Exit Sub
SG_ERR:
    EHandlerADO Err.Number, Err.Description
    GoTo SG_END
End Sub
Public Function DecodeRunStatus(rStat As Long) As String
    Dim x       As String
    x = ""
    Select Case rStat
'    Job activity codes
        Case 0
            x = "Never run"
        Case 10
            x = "Inactive"
        Case 20
            x = "Job in progress"
        Case 30
            x = "Running on-line"
        Case 40
            x = "Submitted"
'   Job completion codes
        Case 100
            x = "Completed without errors"
        Case 110
            x = "Run aborted"
        Case 120
            x = "Run cancelled"
        Case 130
            x = "**ERRORS**"
        Case 140
            x = "Completed no errors but no data"
        Case 150
            x = "Internal error"
        Case 190
            x = "Restarted"
'   Other
        Case 200
            x = "Transmission failure."
        Case Else
            x = "Status undefined"
    End Select
    DecodeRunStatus = x
End Function
Public Sub EHandlerADO(erNum As Long, erTxt As String, _
            Optional optMsgTxt As String)
    
    Dim aObj            As ADODB.Error
    
    Dim aTxt            As String
    Dim eTxt            As String
    Dim mTxt            As String
    Dim a               As Long
    Dim aec             As Long
    Dim e               As Long
    Dim i               As Long
    Dim x               As String
    Dim omp             As Integer
    
    e = 0
    a = 0
    aec = 0
    eTxt = ""
    aTxt = ""
    mTxt = ""
    
    On Error Resume Next    'We don't want to error in here
    
    If Not IsMissing(optMsgTxt) Then
        mTxt = Trim$(NullToS(optMsgTxt))
    End If
    
    If gADOconnOpen Then a = gADOconn.Errors.Count
    e = erNum
    If e <> 0 Then
        eTxt = "(" & e & ") " & Trim$(erTxt)
    End If

    If a > 0 Then
        aTxt = "ADO Errors:"
        For Each aObj In gADOconn.Errors
            i = aObj.Number
            x = Trim$(aObj.Description)
            If i <> 0 And i <> e Then
                aTxt = aTxt & vbCrLf
                aTxt = aTxt & "(" & i & ") " & x
                aec = aec + 1
            End If
        Next
    End If
    
    x = ""
    If Len(mTxt) > 0 Then
        x = mTxt & vbCrLf & vbCrLf
    End If
    If e <> 0 Then x = x & eTxt
    If aec > 0 Then
        If e > 0 Then x = x & vbCrLf & vbCrLf
        x = x & aTxt
    End If
    
    If gJobSeq > 0 Then x = x & " JobSeq=" & gJobSeq
    If gJobID > 0 Then x = x & " JobID=" & gJobID
    If gRunning_As_A_SERVICE Then
        WriteToEventLog x
    End If
    
End Sub
