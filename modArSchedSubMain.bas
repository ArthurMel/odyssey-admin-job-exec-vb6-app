Attribute VB_Name = "modArSchedSubMain"
Option Explicit

Sub Main()

    Dim x                       As String
    Dim t                       As String
    Dim i                       As Long
    Dim b                       As Boolean
    Dim i2                      As Integer
    Dim xLen                    As Long
    
    Dim jobSeq                  As Long
    
    Dim numArgs                 As Long
    Const maxArgs               As Long = 128
    Dim cmdArg(1 To maxArgs)    As Variant
    Dim argIDX                  As Long
    Dim argSwitch               As String
    Dim argValS                 As String

    On Error GoTo SM_ERR
    
    gIniFile = ""
    gDataSource = ""
    gUserID = ""
    gPwd = ""
    gDefID = 0
    gJobID = 0
    gJobSeq = 0
    gRunning_As_A_SERVICE = False
    gJobSchedMode = True
    gADOconnOpen = False
    gDBconnOpen = False
    monitorMode = False
    gMAPI_Signed_ON = False
    gLogonUser = GetNTLogonName()
    gCommandLine = Command()
    consoleAllocated = False
            
    If CalcLen(gCommandLine) > 0 Then
        numArgs = UnStringCommandLine(gCommandLine, maxArgs, cmdArg)
    Else
        App.LogEvent App.Title & " has been executed without any startup job parameters", vbLogEventTypeInformation
        GoTo SM_TERMINATE
    End If
                    
    For argIDX = 1 To numArgs Step 2
        x = NullToS(cmdArg(argIDX + 1))
        xLen = Len(x)
        If xLen > 0 Then
            Select Case cmdArg(argIDX)
                Case "-D", "-DSN", "-DS"
                    gDataSource = x
                Case "-JOBSEQ", "-J", "-SEQ", "-JOB"
                    jobSeq = CLng(x)
                    gJobSeq = jobSeq
                    gJobSchedMode = False
                Case "-DEF"
                    gDefID = CLng(NullToZero(x))
            End Select
        End If
    Next argIDX

'Testing
'gDataSource = "autoreport"
'jobSeq = 775110
'gJobSeq = 775110

    SetADOConn gADOconn, gDataSource, gUserID, gPwd, gADOconnOpen
    If Not gADOconnOpen Then
        EHandlerADO Err.Number, Err.Description, "Cannot open datasource"
        GoTo SM_END
    End If
    
    If gDefID > 0 Then LoadDefaultSettings gDefID
    'sets this for running jobs
    If Not gJobSchedMode Then gRunning_As_A_SERVICE = True
    
'    EMailSignON 'Create a MAPI session
'
'    If Not gMAPI_Signed_ON Then
'        App.LogEvent "MAPI Login failed.", vbLogEventTypeInformation
'        GoTo SM_TERMINATE
'    End If
    
    If Not gJobSchedMode Then
        gRunning_As_A_SERVICE = True
        RunJob jobSeq, False 'Run program in execute mode
        GoTo SM_TERMINATE
    End If
    
SM_END:
    On Error Resume Next
    If IsLoaded("frmMAPI") Then Unload frmMAPI
    GoTo SM_EXIT
SM_TERMINATE:
    On Error Resume Next
    If gMAPI_Signed_ON Then EMailSignOFF
    If IsLoaded("frmMAPI") Then Unload frmMAPI
    End
SM_ERR:
    EHandlerADO Err.Number, Err.Description
    GoTo SM_END
SM_EXIT:
    End
SM_OVER:
End Sub
