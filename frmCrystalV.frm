VERSION 5.00
Object = "{C4847593-972C-11D0-9567-00A0C9273C2A}#8.0#0"; "crviewer.dll"
Begin VB.Form frmCrystalV 
   Caption         =   "Report Viewer"
   ClientHeight    =   5055
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8340
   Icon            =   "frmCrystalV.frx":0000
   LinkTopic       =   "Form1"
   MinButton       =   0   'False
   ScaleHeight     =   5055
   ScaleWidth      =   8340
   StartUpPosition =   3  'Windows Default
   Begin CRVIEWERLibCtl.CRViewer rptViewr 
      Height          =   7815
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   11295
      DisplayGroupTree=   0   'False
      DisplayToolbar  =   -1  'True
      EnableGroupTree =   -1  'True
      EnableNavigationControls=   -1  'True
      EnableStopButton=   -1  'True
      EnablePrintButton=   -1  'True
      EnableZoomControl=   -1  'True
      EnableCloseButton=   -1  'True
      EnableProgressControl=   -1  'True
      EnableSearchControl=   -1  'True
      EnableRefreshButton=   -1  'True
      EnableDrillDown =   -1  'True
      EnableAnimationControl=   -1  'True
      EnableSelectExpertButton=   0   'False
      EnableToolbar   =   -1  'True
      DisplayBorder   =   0   'False
      DisplayTabs     =   0   'False
      DisplayBackgroundEdge=   -1  'True
      SelectionFormula=   ""
      EnablePopupMenu =   -1  'True
      EnableExportButton=   -1  'True
      EnableSearchExpertButton=   0   'False
      EnableHelpButton=   0   'False
   End
End
Attribute VB_Name = "frmCrystalV"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Activate()

    rptViewr.ViewReport
    
End Sub

Private Sub Form_Resize()

    rptViewr.Top = 0
    rptViewr.Left = 0
    rptViewr.Height = ScaleHeight
    rptViewr.Width = ScaleWidth
    
End Sub

Private Sub rptViewr_PrintButtonClicked(UseDefault As Boolean)
Dim PaperOrientation As Integer
Dim PaperSource As Integer

    'Retrieve paper orientation of report
    PaperOrientation = crxiReport.PaperOrientation

    
    'Retrieve paper source of report
    PaperSource = crxiReport.PaperSource
    
    UseDefault = False
  
    On Error GoTo Err:
    'KC Mod 631 7/10/2002
    'If printer not provided, then use settings from printer selection
    'else, use setup printer values
    If Me.Tag = 0 Then 'KC Mod 631
        CMD1.CancelError = True
        With CMD1
            'set the default minimum page
            .Min = 1
            'set the FromPage to be default to the first page:
            .FromPage = .Min
            'set the default maximum page to be the number of pages in the report;
            .Max = crxiReport.PrintingStatus.NumberOfPages
            'set the ToPage to be default to the last page of the report:
            .ToPage = .Max
            'Display printer Dialog control
            .ShowPrinter
            'Set printer based on user selection
            crxiReport.SelectPrinter Printer.DriverName, Printer.DeviceName, Printer.Port
    
        
            'set paper orientation(portrait=1/landscape=2)
            'MUST be set after ".SelectPrinter" function
            crxiReport.PaperOrientation = PaperOrientation
            
            'set paper source
            'MUST be set after ".SelectPrinter" function
            crxiReport.PaperSource = PaperSource
            
            'print the report according the commondialog settings of start page and end page:
            crxiReport.PrintOut False, CMD1.Copies, , CMD1.FromPage, CMD1.ToPage
        End With
    Else
        crxiReport.PrintOut True
    End If  'KC Mod 631
    Exit Sub

Err:
    Exit Sub
End Sub
