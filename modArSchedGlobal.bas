Attribute VB_Name = "modArSchedGlobal"
Option Explicit

Global gADOconn                     As ADODB.Connection   'ADO Connection
Global gADOconn_String              As String   'ADO Connection string
Global gADOconnOpen                 As Boolean

Global gDBconn                      As ADODB.Connection   'ADO Connection
Global gDBconn_String               As String   'ADO Connection string
Global gDBconnOpen                  As Boolean

'================================================
Global gJobSeq                      As Long
Global gJobID                       As Long

Global gExcelApp                    As Object

Global Const maxQueues              As Integer = 128
Global Const maxStreams             As Integer = 128
Global Const gADOTimeOut            As Long = 300   '5 Minutes

Global monitorMode                  As Boolean  'Ask if run on-line msg

Global Const fax_with_FaxWare       As Integer = 1
Global Const fax_with_FaxMaker      As Integer = 2
Global Const Fax_Method             As Integer = fax_with_FaxMaker

'Console output stuff

Public Declare Function AllocConsole Lib "kernel32" () As Long
Public Declare Function FreeConsole Lib "kernel32" () As Long
Public Declare Function GetStdHandle Lib "kernel32" (ByVal nStdHandle As Long) As Long
Public Const STD_INPUT_HANDLE = -10&
Public Const STD_OUTPUT_HANDLE = -11&
Public Const STD_ERROR_HANDLE = -12&
Public Declare Function WriteConsole Lib "kernel32" Alias "WriteConsoleA" (ByVal hConsoleOutput As Long, ByVal lpBuffer As Any, ByVal nNumberOfCharsToWrite As Long, lpNumberOfCharsWritten As Long, lpReserved As Any) As Long
Public Declare Function SetConsoleTitle Lib "kernel32" Alias "SetConsoleTitleA" (ByVal lpConsoleTitle As String) As Long
Public Declare Function SetConsoleTextAttribute Lib "kernel32" (ByVal hConsoleOutput As Long, ByVal wAttributes As Long) As Long
Public Const FOREGROUND_BLUE = &H1 ' text color contains blue.
Public Const FOREGROUND_GREEN = &H2 ' text color contains green.
Public Const FOREGROUND_INTENSITY = &H8 ' text color is intensified.
Public Const FOREGROUND_RED = &H4 ' text color contains red.
Public Const BACKGROUND_BLUE = &H10 ' background color contains blue.
Public Const BACKGROUND_GREEN = &H20 ' background color contains green.
Public Const BACKGROUND_RED = &H40 ' background color contains red.
Public Const BACKGROUND_INTENSITY = &H80 ' background color is intensified
Public Declare Function SetConsoleMode Lib "kernel32" (ByVal hConsoleHandle As Long, ByVal dwMode As Long) As Long
Public Declare Function ReadConsole Lib "kernel32" Alias "ReadConsoleA" (ByVal hConsoleInput As Long, ByVal lpBuffer As Any, ByVal nNumberOfCharsToRead As Long, lpNumberOfCharsRead As Long, lpReserved As Any) As Long
Public Const ENABLE_LINE_INPUT = &H2
Public Const ENABLE_ECHO_INPUT = &H4
Public Const ENABLE_MOUSE_INPUT = &H10
Public Const ENABLE_PROCESSED_INPUT = &H1
Public Const ENABLE_WINDOW_INPUT = &H8
Public Const ENABLE_PROCESSED_OUTPUT = &H1
Public Const ENABLE_WRAP_AT_EOL_OUTPUT = &H2

Global hConsoleIn As Long 'The console's input handle
Global hConsoleOut As Long 'The console's output handle
Global hConsoleErr As Long 'The console's error handle
Global consoleAllocated As Boolean

