Attribute VB_Name = "modCrystalGlobal"
Option Explicit

Global gDBName              As String
Global GDSNAME              As String
Global gADOConnOpen         As Boolean
Global gADOConn             As ADODB.Connection
Global gATTACHDIR           As String
Global Const GSTDDeFrmHeight    As Long = 3075
Global Const GSTDDeFrmWidth     As Long = 7125
Global gJobSeq              As Long
Global gJobID               As Long
