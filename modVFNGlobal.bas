Attribute VB_Name = "modVFNGlobal"
'This SOFTWARE PRODUCT is licensed, not sold, and is protected by international intellectual
'property laws and treaties, and remains the property of Roger Bryant (the OWNER).
'Limitations.
'*   You may not resell the SOFTWARE PRODUCT
'*   You may not rent, lease or lend the SOFTWARE PRODUCT.
'*   You may not copy the SOFTWARE PRODUCT unless this copy is being made for backup purposes.
'*   You may not reverse engineer, decompile or disassemble the SOFTWARE PRODUCT without the
'    written permission of the OWNER.
'
'Failure to meet these terms and conditions will terminate this license agreement.
'Should this event take place, all copies of the SOFTWARE PRODUCT must be destroyed.

Option Explicit

Public Type fNameObj
    FileNameFull    As String
    DirPath         As String
    FileName        As String
    Suffix          As String
End Type

Global GnewFName        As String
Global GoldFName        As String
Global GcancEnabled     As Boolean

Public Function VerifyFileName(txtFName As String, _
                    loadListBox As Boolean) As Boolean

    Dim fl      As Long
    Dim i       As Long
    Dim x       As String * 1
    Dim f       As String
    Dim t       As String
    Dim b       As Boolean
    
    b = True
    If loadListBox Then frmVFN.listErrors.Clear
    f = txtFName
    fl = Len(f)
    If fl > 255 Then
        b = False
        If loadListBox Then
            frmVFN.listErrors.AddItem _
                "File name exceeds 255 characters"
        End If
    End If
    
    For i = 1 To fl
        x = " "
        x = Mid$(f, i, 1)
        If Not ValidFileChar(x) Then
            b = False
            If loadListBox Then
                t = "Illegal character: " & _
                    x & "  at position " & i
                frmVFN.listErrors.AddItem t
            End If
        End If
    Next i
    
    VerifyFileName = b
    
End Function
Public Function CorrectFileName(oldFile As String) As String

    Dim fl      As Long
    Dim i       As Long
    Dim x       As String * 1
    Dim f       As String
    Dim n       As String
    
    f = Left$(oldFile, 255)
    n = ""
    fl = Len(f)
    
    For i = 1 To fl
        x = " "
        x = Mid$(f, i, 1)
        If Not ValidFileChar(x) Then
            x = "_"
        End If
        n = n & x
    Next i
    
    CorrectFileName = n
    
End Function
Public Function ValidFileChar(fileChar As String) As Boolean

    Dim b           As Boolean
    Dim fc          As Integer
    
    fc = Asc(fileChar)
    b = True
    Select Case fc
        Case Is < 32
            b = False
        Case 60, 62, 58, 34, 47, 92, 124 '< > : " / \ |
            b = False
    End Select
    ValidFileChar = b
End Function
Public Function SetFNameObj( _
                    strFName As String, _
                    Optional optstrDir As String, _
                    Optional optstrSuffix As String) As starFNameObj
                    
    Dim o       As starFNameObj
    Dim x       As String
    Dim t       As String
    Dim p       As Long
    Dim l       As Long
    Dim i       As Long
    
    If IsMissing(optstrDir) And IsMissing(optstrSuffix) Then
        GoTo SF_FROM_FULL
    End If
    If CalcLen(optstrDir) = 0 And CalcLen(optstrSuffix) = 0 Then
        GoTo SF_FROM_FULL
    End If
    
    With o
        x = Trim$(NullToS(optstrDir))
        If Right$(x, 1) = "\" Then x = Left$(x, Len(x) - 1)
        .DirPath = x
        x = Trim$(NullToS(strFName))
        .FileName = x
        x = UCase$(Trim$(NullToS(optstrSuffix)))
        If Left$(x, 1) = "." Then x = Right$(x, Len(x) - 1)
        .Suffix = x
        .FileNameFull = .DirPath & "\" & .FileName & "." & .Suffix
    End With
    
    GoTo SF_END
    
SF_FROM_FULL:
    With o
        o.DirPath = ""
        o.FileName = ""
        o.Suffix = ""
        t = Trim$(NullToS(strFName))
        o.FileNameFull = t
    End With
    l = Len(t)
    If l < 1 Then GoTo SF_END
    
'Go backwards and look for a "."
    p = l
    i = 0
    Do Until i > 0 Or p < 1
        If Mid$(t, p, 1) = "." Then
            i = p
        Else
            p = p - 1
        End If
    Loop
    If i <> l And i <> 0 Then o.Suffix = Right$(t, l - i)
    l = i - 1
    If l > 0 Then t = Left$(t, l)
    
'Go backwards look for a "\"
    l = Len(t)
    p = l
    i = 0
    Do Until i > 0 Or p < 1
        If Mid$(t, p, 1) = "\" Then
            i = p
        Else
            p = p - 1
        End If
    Loop
    If i <> l Then o.FileName = Right$(t, l - i)
    l = i - 1
    If l > 0 Then o.DirPath = Left$(t, l)
        
SF_END:
    SetFNameObj = o
    Exit Function
    
End Function
Public Function CheckFileName(inFile As String, _
                    Optional optCancelEnabled As Boolean) As String

    Dim b           As Boolean
    
    On Error Resume Next
    
    GnewFName = inFile
    GoldFName = inFile
    GcancEnabled = False
    If Not IsMissing(optCancelEnabled) Then
        GcancEnabled = optCancelEnabled
    End If
        
    b = VerifyFileName(inFile, False)
    If Not b Then
        frmVFN.Show vbModal
    End If
    CheckFileName = GnewFName
    
End Function
Public Function FixFileName(oldFile As String) As String
    FixFileName = CorrectFileName(oldFile)
End Function







