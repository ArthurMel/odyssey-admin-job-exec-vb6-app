Attribute VB_Name = "modAutoReportSTDRoutines"
Option Explicit

Public Function CreateFilterString(ParamArray varFilt() As Variant) As String
    
    Dim strFilter   As String
    Dim intRet      As Integer
    Dim intNum      As Integer

    intNum = UBound(varFilt)
    If (intNum <> -1) Then
        strFilter = varFilt(0)
        For intRet = 1 To intNum
            strFilter = strFilter & "|" & varFilt(intRet)
        Next
        If intNum Mod 2 = 0 Then
            strFilter = strFilter & "|*.*"
        End If
    Else
        strFilter = ""
    End If

    CreateFilterString = strFilter
    
End Function
Public Function CalcLen(anyString As Variant) As Long
    CalcLen = 0
    On Error Resume Next

    If IsNull(anyString) Then
        CalcLen = 0
    Else
        CalcLen = Len(RTrim$(anyString))
    End If
End Function
Public Function NullToS(anyVal As Variant) As String
    If IsNull(anyVal) Then
        NullToS = ""
    Else
        If Len(anyVal) > 0 Then
            NullToS = anyVal
        Else
            NullToS = ""
        End If
    End If
End Function
Public Function NullToZero(anyValue As Variant) As Variant
    If CalcLen(anyValue) < 1 Or IsNull(anyValue) Then
        NullToZero = 0
    Else
        NullToZero = anyValue
    End If
End Function
