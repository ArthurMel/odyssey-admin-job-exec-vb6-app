Attribute VB_Name = "modCrystalAuto"
Option Explicit

Public Sub ScheduleJobs()

    Dim rs                  As ADODB.Recordset
    
End Sub
Public Sub SetGlobalADOConn()
    
    Dim cs          As String
    Dim db          As ADODB.Connection
    
    On Error GoTo SG_ERR
    
    gADOConnOpen = False
    Set gADOConn = New ADODB.Connection
    gADOConn.CursorLocation = adUseClient
    
    cs = 1
    
    Select Case cs
        Case 1
            cs = "PROVIDER=MSDASQL;" & _
                "DSN=" & GDSNAME & ";UID=;PWD=;"
        Case 2
            cs = "DSN=" & GDSNAME & ";UID=;PWD=;"
    End Select
    
    gADOConn.Open cs
    gADOConnOpen = True
    
SG_END:
    Exit Sub
SG_ERR:
    EHandler Err.Number, Err.Description
    GoTo SG_END
End Sub
Public Sub EHandler(erNum As Long, erTxt As String, _
            Optional optMsgTxt As String)
    
    Dim aObj            As ADODB.Error
    
    Dim aTxt            As String
    Dim eTxt            As String
    Dim mTXT            As String
    Dim a               As Long
    Dim aec             As Long
    Dim e               As Long
    Dim i               As Long
    Dim x               As String
    Dim omp             As Integer
    
    omp = Screen.MousePointer
    Screen.MousePointer = vbNormal
    e = 0
    a = 0
    aec = 0
    eTxt = ""
    aTxt = ""
    mTXT = ""
    
    On Error Resume Next    'We don't want to error in here
    
    If Not IsMissing(optMsgTxt) Then
        mTXT = Trim$(NullToS(optMsgTxt))
    End If
    
    If gADOConnOpen Then a = gADOConn.Errors.Count
    e = erNum
    If e <> 0 Then
        eTxt = "(" & e & ") " & Trim$(erTxt)
    End If

    If a > 0 Then
        a = "ADO Errors:"
        For Each aObj In gADOConn.Errors
            i = aObj.Number
            x = Trim$(aObj.Description)
            If i <> 0 And i <> e Then
                aTxt = aTxt & vbCrLf
                aTxt = aTxt & "(" & i & ") " & x
                aec = aec + 1
            End If
        Next
    End If
    
    x = ""
    If Len(mTXT) > 0 Then
        x = mTXT & vbCrLf & vbCrLf
    End If
    If e <> 0 Then x = x & eTxt
    If aec > 0 Then
        If e > 0 Then x = x & vbCrLf & vbCrLf
        x = x & aTxt
    End If
    
    MsgBox x, vbExclamation + vbOKOnly, App.Title
    Screen.MousePointer = omp
    
End Sub
Public Function NullToS(anyThing As Variant) As String
    If IsNull(anyThing) Then
        NullToS = ""
    Else
        NullToS = CStr(anyThing)
    End If
    Exit Function
End Function
Public Function NullToZero(anyThing As Variant) As Variant
    If IsNull(anyThing) Then
        NullToZero = 0
    Else
        If Len(anyThing) = 0 Then
            NullToZero = 0
        Else
            NullToZero = anyThing
        End If
    End If
    Exit Function
End Function
Public Function CreateFilterString(ParamArray varFilt() As Variant) As String
    
    Dim strFilter   As String
    Dim intRet      As Integer
    Dim intNum      As Integer

    intNum = UBound(varFilt)
    If (intNum <> -1) Then
        strFilter = varFilt(0)
        For intRet = 1 To intNum
            strFilter = strFilter & "|" & varFilt(intRet)
        Next
        If intNum Mod 2 = 0 Then
            strFilter = strFilter & "|*.*"
        End If
    Else
        strFilter = ""
    End If

    CreateFilterString = strFilter
    
End Function


