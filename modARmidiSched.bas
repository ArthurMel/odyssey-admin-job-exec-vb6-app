Attribute VB_Name = "modARmidiSched"
Option Explicit

Public Sub RunJob(jobSeq As Long, runOnLineOverride As Boolean)

    Dim rs              As ADODB.Recordset
    Dim rsOpen          As Boolean
    Dim rsSQL           As String
    
    Dim r               As Long
    Dim i               As Long
    Dim jobID           As Long
    Dim nRecs           As Long
    Dim mTxt            As String
    Dim osp             As Integer
    Dim sTim            As Date
    Dim eTim            As Date
    
    rsOpen = False
    nRecs = 0
    
    On Error GoTo RJ_ERR
    
    rsSQL = "select ars.arsched_id,ars.arjob_id,arj.arjob_desc " & _
        "from auto_report_scheduler as ars inner join " & _
        "auto_report_jobs as arj on ars.arjob_id=arj.arjob_id " & _
        "where ars.arsched_id=" & jobSeq
    Set rs = New ADODB.Recordset
    rs.Open rsSQL, gADOconn, adOpenForwardOnly, adLockReadOnly
    rsOpen = True
    
    With rs
        If .EOF Then
            WriteToJobHistory jobSeq, "Error locating job for execution", 0, 150
            rsSQL = "UPDATE auto_report_scheduler " & _
                "SET arrunstat_id = 150, arsched_flag = 5 " & _
                "WHERE arsched_id = " & jobSeq
            gADOconn.Execute rsSQL
            GoTo RJ_END
        End If
        jobID = ![arjob_id]
        gJobID = jobID
        .Close
        rsOpen = False
    End With
    
    sTim = Now()
    rsSQL = "UPDATE auto_report_scheduler " & _
                "SET arsched_started = getdate(), " & _
                "arsched_flag = 3, arrunstat_id = 20 " & _
                "WHERE arsched_id = " & jobSeq
    gADOconn.Execute rsSQL
        
    WriteToJobHistory jobID, "Job started. Sequence=" & jobSeq, 0, 20
    RunCrystalReport jobID, jobSeq, mTxt, vbYes, nRecs, runOnLineOverride
         
    If nRecs > 0 Then
        r = 10
    Else
        r = 140 'for no data
    End If

    eTim = Now()
    rsSQL = "UPDATE auto_report_scheduler " & _
                "SET arsched_completed = getdate() " & _
                ", arsched_flag = 9 " & _
                ", arrunstat_id = " & r & " " & _
                "WHERE arsched_id = " & jobSeq
    gADOconn.Execute rsSQL
        
    i = DateDiff("d", sTim, eTim)
    WriteToJobHistory jobID, "Job completed. Sequence=" & jobSeq, i, r
            
RJ_END:
    On Error Resume Next
    If rsOpen Then
        rsOpen = False
        rs.Close
    End If
    Set rs = Nothing
    Exit Sub
RJ_ERR:
    EHandlerADO Err.Number, Err.Description
    GoTo RJ_END
                
End Sub

